﻿FORMAT: 1A
HOST: http://example.example.org/

# ИЛЬ ДЕ БОТЭ API

## API Reference

API орагнизовано по принципам REST. Данные запросов и ответов передаются в формате JSON в кодировке UTF-8. 
Для описания структуры JSON используется JSON Schema (http://json-schema.org).
Для индикации типов ошибок используются коды возврата HTTP.

## Versioning

API версионируется в соответствии со стандартом семантического версионирования 
(http://semver.org). Для указания необходимой версии используется 
HTTP-заголовок "User-Agent" (http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html см. п.14.43).
Пример:

        User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; UTC+3; iphone6splus) API/0.2
        User-Agent: IledebeauteMobileApp (andriod/9.2; mobile/1.2.3; UTC+6; apib-client) API/0.5

Первый комментарий заголовка описывает тип клиента: "IledebeauteMobileApp".
Версия API указывается в комментарии заголовка в формате "API/х.х". 
Версия патча (0.0.*) используется только для различения версий серверной части,
чтобы не нужно было модифицировать клиентскую часть при исправлении багов в серверной части.

В скобках указываются дополнительные параметры клиента.
Дополнительные параметры разделяются точкой с запятой и пробелом.
В начале идут три обязательных параметра строго в таком порядке:

— ОС устройства и ее версия: "ios/9.2" (все буквы - строчные)

— тип приложения и его версия: "mobile/1.2.3" (все буквы - строчные)

— часовой пояс устройства: "UTC+3" (варианты: "UTC+3", "UTC-12")

Далее идут 0 или больше необязательных параметров в произвольном порядке.

## Authentication

Для аутентификации используется HTTP Basic Authentication Scheme (https://www.ietf.org/rfc/rfc2617.txt). 
Перед тем, как начать использовать API, необходимо получить **session_id** и **api_token**. 
Получив их, можно авторизоваться, используя заголовок Authorization, 
ключевое слово Basic и base64-кодированную строку "session_id:api_token" 
(обязательно двоеточие между session_id и api_token). Например:

        Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

В будущем планируется перейти на использование HTTP Digest Access Authentication Scheme.

## Errors

**200 (OK)** — в случае успешного выполнения запроса.

**400 (Bad Request)** — может быть возвращен в ответе на любой запрос, в котором передавались данные.
В теле ответа будет json со списком описаний найденных ошибок:

        {
            "errors": [
                {
                    "code": 1,
                    "message": "Несоответствие запроса схеме данных",
                    "field": "email"
                },
                {...},
                ...
            ]
        }

json схема для этого ответа:

        {
            "$schema": "http://json-schema.org/draft-04/schema#",
            "type": "object",
            "properties": {
                "errors": {
                    "type": "array",
                    "uniqueItems": true,
                    "description": "Список найденных ошибок",
                    "items": {
                        "type": "object",
                        "description": "Ошибка",
                        "properties": {
                            "code": {
                                "type": "integer",
                                "description": "Код ошибки"
                            },
                            "message": {
                                "type": "string",
                                "maxLength": 128,
                                "description": "Описание ошибки"
                            },
                            "field": {
                                "type": "string",
                                "maxLength": 16,
                                "description": "Поле в котором произошла ошибка"
                            }
                        },
                        "additionalProperties": false,
                        "required": ["code", "message"]
                    }
                }
            },
            "additionalProperties": false,
            "required": ["errors"]
        }
    


В **code** указан заранее выбранный код для данной ошибки (список кодов: https://goo.gl/x7uhoQ), 
поле **message** содержит описание ошибки на английском языке, 
а поле **field** - имя поля, в котором была ошибка. Если ошибка не относится к какому-то 
конкретному полю, **field** может быть опущено.
В описании методов приводятся все возможные ошибки для каждого из методов.

**401 (Unauthorized)** — может быть возвращен в ответе на любой запрос, если проверка авторизации 
устройства не сработала. В таком случае клиенту нужно пройти авторизацию устройства. 
Тело ответа пустое.

**403 (Forbidden)** — при обращении к методам, требующим предварительной авторизации пользователя.
Тело ответа пустое.

**404 (Not Found)** — при обращении к несуществующему ресурсу. Тело ответа пустое.

**405 (Method Not Allowed)** — при обращении к методу, 
который недоступен для данного ресурса. Тело ответа пустое.

**500 (Internal Server Error)** — в случае внутренней ошибки сервера. Тело ответа пустое.
Может быть ответом на любой запрос клиента, в описании API опускается.

**503 (Service Unavailable)** — сервер недоступен. Тело ответа пустое.
Может быть ответом на любой запрос клиента, в описании API опускается.

Все ошибки могут быть разделены на несколько уровней.
Если произошли ошибки разных уровней, то возвращаются ошибки только самого высокого уровня.
Если произошли ошибки одного уровня, они возвращаются списком.

1. ресурса не существует - 404
2. к ресурсу применен недопустимый HTTP метод - 405
3. устройство неавторизовано - 401 (если такая авторизация нужна для выполнения метода)
4. пользователь неавторизован - 403 (если такая авторизация нужна для выполнения метода)
5. пришли неверные данные от клиента - 400 (если данные передавались)
    - несоответствие запроса схеме данных
    - несоответствие одного поля ограничениям, накладываемым на него
    - несоответствие группы полей комплексным ограничениям, накладываемым на них
6. несоответствие переданных данных другим данным в системе (например, пара логин+пароль не подходят, 
или если после успешной авторизации снова вызывается метод авторизации)
        
## Actions

Для непоследовательной навигации внутри приложения используются actions, 
которые могут применяться в нескольких случаях:

— переход с push-уведомления

— переход с персональной новости

— переход по ключевому слову, набранному в поиске (например "шанель" -> раздел бренда Chanel)

— переход с баннера

— переход с кнопки в описании акции

— переход с внешней рекламы при использовании технологии deep linking

Описание спецификации push-уведомлений и списка возможных actions вынесено в отдельный документ: 
https://bitbucket.org/Danil_Perevalov/ldbmobile/src/53b8dcb0b21535b7097b34b942d2069a71db8c9a/actions.md?at=master&fileviewer=file-view-default

## {?} Images

Требования к картинкам:
https://docs.google.com/spreadsheets/d/1U0kCHunrwK3nBbbmuUTAqAG0XA5-mWk4DuhIxjA__IQ/edit

Сейчас требования основаны на прототипах, размеры большинства изображений могут поменяться во время согласования дизайна, но незначительно.

Значительно могут измениться размеры баннеров: сегодня приняли решение существенно переработать главный экран приложения, поэтому, а также добавить баннеры на некоторых других экранах, поэтому работа с баннерами будет в ближайшее время значительно пересмотрена.

Размеры картинок на планшетах мы пока не можем зафиксировать, поскольку даже приблизительно не согласовали прототипы многих экранов. Но мы будем стараться использовать те же размеры, что и для смартфонов.


# Group Device

## /device
device_id — строка, идентифицирующая устройство, с которого запущено приложение. 
Нет гарантии, что она будет уникальной для разных устройств. 
Поэтому при авторизации устройства сервер всегда должен считать, что подключилось новое устройство.

### POST - авторизация устройства [POST]

+ Request (application/json)
   + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2

    + Body

            {
                "device_id": "f07a13984f6d116a"
            }
            
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "device_id": {
                        "type": "string",
                        "minLength": 32,
                        "maxLength": 255,
                        "description": "Идентификатор устройства"
                    }
                },
                "additionalProperties": false,
                "required": ["device_id"]
            }

+ Response 200 (application/json)
    + Body

            {
                "session_id": 9,
                "api_token": "352ea2216ed13bfa09a594fc4f538c988ed76c3a"
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "session_id": {
                        "type": "integer",
                        "description": "идентификатор сессии"
                    },
                    "api_token": {
                        "type": "string",
                        "minLength": 32,
                        "maxLength": 60,
                        "description": "токен для подписи идентификатора сессии"
                    }
                },
                "additionalProperties": false,
                "required": ["session_id","api_token"]
            }
            
+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    }
                ]
            }

## /device/push_token

### PUT - отправка токена устройства для сервиса push-уведомлений [PUT]

+ Request (application/json)
    + Headers
    
            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body
    
            {
                "push_token": "740f4707bebcf74f9b7c25d48e3358945f6aa01da5ddb387462c7eaf61bb78ad"
            }
    
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "push_token": {
                        "type": "string",
                        "maxLength": 128,
                        "description": "токен для пуш-уведомлений"
                    }
                },
                "additionalProperties": false,
                "required": ["push_token"]
            }


+ Response 200 (application/json)

+ Response 400 (application/json)
    + Body
    
            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    }
                ]
            }

+ Response 401 (application/json)

# Group User

## /user

### POST - регистрация нового пользователя [POST]

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body
    
            {
                "login": "mail@gmail.com",
                "password": "12345",
                "first_name": "Иван",
                "last_name": "Иванов"
            }
            
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "login": {
                        "type": "string",
                        "format": "email",
                        "description": "логин (пока - только электронная почта)"
                    },
                    "password": {
                        "type": "string",
                        "minLength": 6,
                        "maxLength": 64,
                        "description": "пароль"
                    },
                    "first_name": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "pattern": "(*UTF8)^[А-яЁё]{1,64}$",
                        "description": "имя (только русские символы)"
                    },
                    "last_name": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "pattern": "(*UTF8)^[А-яЁё]{1,64}$",
                        "description": "фамилия (только русские символы)"
                    }
                },
                "additionalProperties": false,
                "required": ["login","password","first_name","last_name"]
            }

+ Response 200 (application/json)

+ Response 400 (application/json)
    + Body
    
            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "error_code": 11,
                        "message": "Такой email уже зарегистрирован"
                    },
                    {
                        "error_code": 22,
                        "message": "Недопустимый метод: пользователь уже авторизован"
                    }
                ]
            }

+ Response 401 (application/json)

### PUT - изменение данных профиля пользователя [PUT]

+ Request (application/json)
    + Headers
    
            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body
    
            {
                "first_name": "Иван",
                "last_name": "Иванов",
                "dob": "2015-11-30",
                "sex": 1,
                "city": "Омск"
            }
            
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "first_name": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "pattern": "(*UTF8)^[А-яЁё]{1,64}$",
                        "description": "имя (только русские буквы)"
                    },
                    "last_name": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "pattern": "(*UTF8)^[А-яЁё]{1,64}$",
                        "description": "фамилия (только русские буквы)"
                    },
                    "dob": {
                        "type": "string",
                        "format": "date",
                        "description": "дата рождения (YYYY-MM-DD)"
                    },
                    "sex": {
                        "type": "number",
                        "enum": [0, 1, 2],
                        "description": "пол: 0 - не определен, 1 - женский, 2 - мужской"
                    },
                    "city": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "description": "город"
                    }
                },
                "additionalProperties": false,
                "required": ["first_name","last_name","dob","sex","city"]
            }

+ Response 200 (application/json)

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "error_code": 13,
                        "message": "Возраст меньше 18 лет"
                    },
                    {
                        "error_code": 14,
                        "message": "Возраст больше 80 лет"
                    }
                ]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

### GET - получение данных профиля [GET]

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)
    + Body

            {
                "first_name": "Иван",
                "last_name": "Иванов",
                "dob": "2015-11-30",
                "sex": 1,
                "city": "Омск"
            }
    
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "first_name": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "pattern": "(*UTF8)^[А-яЁё]{1,64}$",
                        "description": "имя (только русские буквы)"
                    },
                    "last_name": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "pattern": "(*UTF8)^[А-яЁё]{1,64}$",
                        "description": "фамилия (только русские буквы)"
                    },
                    "dob": {
                        "type": "string",
                        "format": "date",
                        "description": "дата рождения (YYYY-MM-DD)"
                    },
                    "sex": {
                        "type": "number",
                        "enum": [0, 1, 2],
                        "description": "пол (0 - не выбран, 1 - женский, 2 - мужской)"
                    },
                    "city": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "description": "город"
                    }
                },
                "additionalProperties": false,
                "required": ["first_name","last_name","dob","sex","city"]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)


## /user/login

### POST - авторизация пользователя [POST]

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "login": "mail@gmail.com",
                "password": "12345"
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "login": {
                        "type": "string",
                        "format": "email",
                        "description": "логин - пока только электронная почта"
                    },
                    "password": {
                        "type": "string",
                        "minLength": 6,
                        "maxLength": 64,
                        "description": "пароль"
                    }
                },
                "additionalProperties": false,
                "required": ["login", "password"]
            }
                
+ Response 200 (application/json)

+ Response 400 (application/json)
    + Body
    
            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "error_code": 10,
                        "message": "Неправильный email или пароль"
                    },
                    {
                        "error_code": 22,
                        "message": "Недопустимый метод: пользователь уже авторизован"
                    }
                ]
            }
                
+ Response 401 (application/json)

## /user/logout

### POST - деавторизация пользователя [POST]

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==
    
+ Response 200 (application/json)

+ Response 400 (application/json)
    + Body
    
            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    }
                ]
            }
                
+ Response 401 (application/json)

+ Response 403 (application/json)

## /user/password

### PUT - смена пароля [PUT]

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "old_password": "12345",
                "new_password": "123456"
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "old_password": {
                        "type": "string",
                        "minLength": 6,
                        "maxLength": 64,
                        "description": "старый пароль"
                    },
                    "new_password": {
                        "type": "string",
                        "minLength": 6,
                        "maxLength": 64,
                        "description": "новый пароль"
                    }
                },
                "additionalProperties": false,
                "required": ["old_password", "new_password"]
            }

+ Response 200 (application/json)

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "error_code": 12,
                        "message": "Новый пароль идиентичен старому"
                    },
                    {
                        "error_code": 16,
                        "message": "Неправильный пароль"
                    }
                ]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

## /user/password/restore

### POST - восстановление пароля [POST]

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "login": "mail@gmail.com"
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "login": {
                        "type": "string",
                        "format": "email",
                        "description": "логин - пока только электронная почта"
                    }
                },
                "additionalProperties": false,
                "required" : ["login"]
            }

+ Response 200 (application/json)

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 15,
                        "message": "Нет пользователя с таким email"
                    }
                ]
            }

+ Response 401 (application/json)

## /user/notifications/email

### PUT - сохранение нового состояния email-подписки [PUT]

+ Request 
    + Headers
    
            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "on": true,
                "email": "mail@gmail.com",
                "idb_news": true,
                "idb_events": true,
                "partner_news": true,
                "cart": true,
                "wishlist": true,
                "birthday_gift": true
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "on": {
                        "type": "boolean",
                        "description": "включена ли рассылка по email"
                    },
                    "email": {
                        "type": "string",
                        "format": "email",
                        "description": "адрес для рассылки"
                    },
                    "idb_news": {
                        "type": "boolean",
                        "description": "новости и акции ИЛЬ ДЕ БОТЭ"
                    },
                    "idb_events": {
                        "type": "boolean",
                        "description": "мероприятия и события в ИЛЬ ДЕ БОТЭ"
                    },
                    "partner_news": {
                        "type": "boolean",
                        "description": "новости партнеров"
                    },
                    "cart": {
                        "type": "boolean",
                        "description": "корзина"
                    },
                    "wishlist": {
                        "type": "boolean",
                        "description": "вишлист"
                    },
                    "birthday_gift": {
                        "type": "boolean",
                        "description": "подарок на день рождения"
                    }
                },
                "additionalProperties": false,
                "required": ["on", "email", "idb_news", "idb_events", "partner_news", "cart", "wishlist", "birthday_gift"]
            }

+ Response 200 (application/json)

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    }
                ]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

### GET - получение текущего статуса email-подписок [GET]

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)
    + Body

            {
                "on": true,
                "email": "mail@gmail.com",
                "idb_news": true,
                "idb_events": true,
                "partner_news": true,
                "cart": true,
                "wishlist": true,
                "birthday_gift": true
            }
            
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "on": {
                        "type": "boolean",
                        "description": "включена ли рассылка по email"
                    },
                    "email": {
                        "type": "string",
                        "format": "email",
                        "description": "адрес для рассылки"
                    },
                    "idb_news": {
                        "type": "boolean",
                        "description": "новости и акции ИЛЬ ДЕ БОТЭ"
                    },
                    "idb_events": {
                        "type": "boolean",
                        "description": "мероприятия и события в ИЛЬ ДЕ БОТЭ"
                    },
                    "partner_news": {
                        "type": "boolean",
                        "description": "новости партнеров"
                    },
                    "cart": {
                        "type": "boolean",
                        "description": "корзина"
                    },
                    "wishlist": {
                        "type": "boolean",
                        "description": "вишлист"
                    },
                    "birthday_gift": {
                        "type": "boolean",
                        "description": "подарок на день рождения"
                    }
                },
                "additionalProperties": false,
                "required": ["on", "email", "idb_news", "idb_events", "partner_news", "cart", "wishlist", "birthday_gift"]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

## /user/notifications/push

### PUT - сохранение нового состояния подписки на push-уведомления [PUT]

+ Request 
    + Headers
    
            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "on": true,
                "service": true,
                "order_status": true,
                "products_availability": true,
                "offers": true,
                "personal_offers": true
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "on": {
                        "type": "boolean",
                        "description": "включена ли рассылка на push-уведомления"
                    },
                    "service": {
                        "type": "boolean",
                        "description": "новости и акции ИЛЬ ДЕ БОТЭ"
                    },
                    "order_status": {
                        "type": "boolean",
                        "description": "мероприятия и события в ИЛЬ ДЕ БОТЭ"
                    },
                    "products_availability": {
                        "type": "boolean",
                        "description": "новости партнеров"
                    },
                    "offers": {
                        "type": "boolean",
                        "description": "корзина"
                    },
                    "personal_offers": {
                        "type": "boolean",
                        "description": "вишлист"
                    }
                },
                "additionalProperties": false,
                "required": ["on", "service", "order_status", "products_availability", "offers", "personal_offers"]
            }

+ Response 200 (application/json)

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    }
                ]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

### GET - получение текущего статуса подписки на push-уведомления [GET]

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)
    + Body

            {
                "on": true,
                "service": true,
                "order_status": true,
                "products_availability": true,
                "offers": true,
                "personal_offers": true
            }
            
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "on": {
                        "type": "boolean",
                        "description": "включена ли рассылка на push-уведомления"
                    },
                    "service": {
                        "type": "boolean",
                        "description": "новости и акции ИЛЬ ДЕ БОТЭ"
                    },
                    "order_status": {
                        "type": "boolean",
                        "description": "мероприятия и события в ИЛЬ ДЕ БОТЭ"
                    },
                    "products_availability": {
                        "type": "boolean",
                        "description": "новости партнеров"
                    },
                    "offers": {
                        "type": "boolean",
                        "description": "корзина"
                    },
                    "personal_offers": {
                        "type": "boolean",
                        "description": "вишлист"
                    }
                },
                "additionalProperties": false,
                "required": ["on", "service", "order_status", "products_availability", "offers", "personal_offers"]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

## /user/emails
Есть один основной email (main) и ноль или несколько дополнительных.
Дополнительные email-ы могут быть подтвержденными или неподтвержденными.

### GET - получение списка email-ов [GET]

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)

    + Body
    
            {
                "emails": [
                    {
                        "email": "mail1@gmail.com",
                        "main": true,
                        "confirmed": false
                    },
                    {
                        "email": "mail3@gmail.com",
                        "main": false,
                        "confirmed": true
                    },
                    {
                        "email": "mail3@gmail.com",
                        "main": false,
                        "confirmed": false
                    }
                ]
            }
    
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "emails": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "email-ы",
                        "items": {
                            "type": "object",
                            "description": "email",
                            "properties": {
                                "email": {
                                    "type": "string",
                                    "format": "email",
                                    "description": "email"
                                },
                                "main": {
                                    "type": "boolean",
                                    "description": "главный email (может быть только один в списке)"
                                },
                                "confirmed": {
                                    "type": "boolean",
                                    "description": "статус подтверждения"
                                }
                            },
                            "additionalProperties": false,
                            "required": ["email","main","confirmed"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["emails"]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

### POST - добавление нового email [POST]

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "email": "example@example.com"
            }
            
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "email": {
                        "type": "string",
                        "format": "email",
                        "description": "электронная почта"
                    }
                },
                "additionalProperties": false,
                "required": ["email"]
            }

+ Response 200 (application/json)

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 11,
                        "message": "Такой email уже зарегистрирован"
                    }
                ]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

## /user/emails/main
Основной email можно изменить только на другой подтвержденный email.

### GET - получение основного email [GET]

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)
    + Body

            {
                "email": "mail@gmail.com"
            }
    
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "email": {
                        "type": "string",
                        "format": "email",
                        "description": "основной email, привязанный к аккаунту"
                    }
                },
                "additionalProperties": false,
                "required": ["email"]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

### PUT - изменение основного email [PUT]

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "email": "mail@gmail.com"
            }
            
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "email": {
                        "type": "string",
                        "format": "email",
                        "description": "электронная почта"
                    }
                },
                "additionalProperties": false,
                "required": ["email"]
            }

+ Response 200 (application/json)

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 17,
                        "message": "Новый email совпадает со старым"
                    },
                    {
                        "code": 18,
                        "message": "Новый email еще не подтвержден"
                    }
                ]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

## /user/emails/delete

### POST - удаление email из списка [POST]

+ Request (application/json)
    + Headers
    
            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "email": "mail@gmail.com"
            }
            
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "email": {
                        "type": "string",
                        "format": "email",
                        "description": "электронная почта"
                    }
                },
                "additionalProperties": false,
                "required": ["email"]
            }

+ Response 200 (application/json)

+ Response 400 (application/json)

    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 19,
                        "message": "Нет такого email-а"
                    },
                    {
                        "code": 20,
                        "message": "Основной email нельзя удалять"
                    }
                ]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

## /user/emails/confirm

### POST - отправка письма со ссылкой на подтверждение email [POST]

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "email": "mail@gmail.com"
            }
            
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "email": {
                        "type": "string",
                        "format": "email",
                        "description": "электронная почта"
                    }
                },
                "additionalProperties": false,
                "required": ["email"]
            }

+ Response 200 (application/json)

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 19,
                        "message": "Нет такого email-а"
                    }
                ]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

## /user/news

### POST - получение списка новостей пользователя [POST]

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "count": 20,
                "start_after": 233405 // необязательный параметр
            }
            
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "count": {
                        "type": "integer",
                        "minimum": 1,
                        "description": "количество новостей"
                    },
                    "start_after": {
                        "type": "integer",
                        "minimum": 1,
                        "description": "id новости, после которой нужно возвратить следующий блок новостей, сама эта новость включаться не должна"
                    }
                },
                "additionalProperties": false,
                "required": ["count"]
            }

+ Response 200 (application/json)

    + Body
    
            {
                "unread": 2,
                "news": [
                    {
                        "id": 233405,
                        "date": "2015-12-11",
                        "text": "Специально для вас Chanel со скидкой до 30%",
                        "action": {
                            // см. описание Actions
                        },
                        "read": false,
                        "promocode": "FWJELTL" // необязательный параметр
                    }
                ]
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "unread": {
                        "type": "integer",
                        "minimum": 0,
                        "description": "общее количество непрочитанных новостей"
                    },
                    "news": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "блок новостей",
                        "items": {
                            "type": "object",
                            "description": "Новость",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "minimum": 1,
                                    "description": "идентификатор новости"
                                },
                                "date": {
                                    "type": "string",
                                    "format": "date",
                                    "description": "дата появления новости (YYYY-MM-DD)"
                                },
                                "text": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 150,
                                    "description": "текст новости"
                                },
                                "action": {
                                    // см. описание Actions
                                },
                                "read": {
                                    "type": "boolean",
                                    "description": "просмотрена ли новость пользователем"
                                },
                                "promocode": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 16,
                                    "description": "промо-код на скидку или с подарком"
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "date", "text", "action", "read"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["news"]
            }

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    }
                ]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

## /user/news/{id}/read

### POST - новость просмотрена пользователем [POST]
+ Parameters
    + id: 123 (required, number) - ID новости

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)

+ Response 401 (application/json)

+ Response 403 (application/json)

+ Response 404 (application/json)

# Group Feedback

## /feedback

### POST - отправка формы обратной связи [POST]

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "category_id": 8,
                "message": "example_example_example_example",
                "email": "example@example.com"
            }
            
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "category_id": {
                        "type": "integer",
                        "minimum": 1,
                        "description": "идентификатор категории"
                    },
                    "message": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 4096,
                        "description": "текст сообщения"
                    },
                    "email": {
                        "type": "string",
                        "format": "email",
                        "description": "email для получения уведомлений"
                    }
                },
                "additionalProperties": false,
                "required": ["category_id", "message", "email"]
            }

+ Response 200 (application/json)

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    }
                ]
            }

+ Response 401 (application/json)

## /feedback/categories

### GET - получение списка категорий [GET]

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)
    + Body

            {
                "groups": [
                    {
                        "group_name": "Магазины ИЛЬ ДЕ БОТЭ",
                        "categories": [
                            {
                                "category_id": 1,
                                "category_name": "Качество обслуживания"
                            },
                            {
                                "category_id": 2,
                                "category_name": "Дисконтная программа"
                            }
                        ]
                    },
                    {
                        "group_name": "Портал ILDEBEAUTE.RU",
                        "categories": [
                            {
                                "category_id": 6,
                                "category_name": "Конкурсы и призы"
                            },
                            {
                                "category_id": 7,
                                "category_name": "Работа сайта"
                            }
                        ]
                    }
                ]
            }
    
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "groups": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "группы категорий",
                        "items": {
                            "type": "object",
                            "description": "группа категорий",
                            "properties": {
                                "group_name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название группы"
                                },
                                "categories": {
                                    "type": "array",
                                    "description": "категории",
                                    "items": {
                                        "type": "object",
                                        "properties": {
                                            "category_id": {
                                                "type": "integer",
                                                "description": "идентификатор категории"
                                            },
                                            "category_name": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 128,
                                                "description": "название категории"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["category_id", "category_name"]
                                    }
                                }
                            },
                            "additionalProperties": false,
                            "required": ["group_name", "categories"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["groups"]
            }

+ Response 401 (application/json)

# Group Catalog

## /catalog

### GET - получение списка категорий [GET]

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==
    
+ Response 200 (application/json)
    + Body

            {
                "catalog": [
                    {
                        "id": 1,
                        "name": "Ароматы",
                        "subs": [
                            {
                                "id": 101,
                                "name": "Женские ароматы",
                                "subs": []
                            },
                            {
                                "id": 102,
                                "name": "Мужские ароматы",
                                "subs": []
                            },
                            {
                                "id": 103,
                                "name": "Ароматы унисекс",
                                "subs": []
                            },
                            {
                                "id": 104,
                                "name": "Наборы",
                                "subs": []
                            }
                        ]
                    },
                    {
                        "id": 2,
                        "name": "Макияж",
                        "subs": [
                            {
                                "id": 201,
                                "name": "Тон",
                                "subs": [
                                    {
                                        "id": 20101,
                                        "name": "База/Основа"
                                    },
                                    {
                                        "id": 20102,
                                        "name": "Тональный крем"
                                    },
                                    {
                                        "id": 20103,
                                        "name": "Компактная пудра"
                                    },
                                    {
                                        "id": 20104,
                                        "name": "Рассыпчатая пудра"
                                    },
                                    {
                                        "id": 20105,
                                        "name": "Корректор"
                                    },
                                    {
                                        "id": 20106,
                                        "name": "Средства с эффектом загара"
                                    },
                                    {
                                        "id": 20107,
                                        "name": "Матирующие салфетки"
                                    },
                                    {
                                        "id": 20108,
                                        "name": "Румяна"
                                    },
                                    {
                                        "id": 20108,
                                        "name": "Хайлайтеры / люминайзеры"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
    
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "catalog": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список категорий каталога 1 уровня",
                        "items": {
                            "type": "object",
                            "description": "категория каталога 1 уровня",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор категории"
                                },
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название категории"
                                },
                                "subs": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список подкатегорий второго уровня",
                                    "items": {
                                        "type": "object",
                                        "description": "подкатегория второго уровня",
                                        "properties": {
                                           "id": {
                                                "type": "integer",
                                                "description": "идентификатор подкатегории"
                                            },
                                            "name": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 128,
                                                "description": "название подкатегории"
                                            },
                                            "subs": {
                                                "type": "array",
                                                "uniqueItems": true,
                                                "description": "список подкатегорий третьего уровня",
                                                "items": {
                                                    "type": "object",
                                                    "description": "подкатегория третьего уровня",
                                                    "properties": {
                                                        "id": {
                                                            "type": "integer",
                                                            "description": "идентификатор подкатегории"
                                                        },
                                                        "name": {
                                                            "type": "string",
                                                            "minLength": 1,
                                                            "maxLength": 128,
                                                            "description": "название подкатегории"
                                                        }
                                                    },
                                                    "additionalProperties": false,
                                                    "required": ["id", "name"]
                                                }
                                            } 
                                        },
                                        "additionalProperties": false,
                                        "required": ["id", "name"]
                                    }
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "name", "subs"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["catalog"]
            }

+ Response 401 (application/json)

# /catalog/way
Возвращает часть каталога, в которой есть товары, соответствующие определенному фильтру.
Метод нужен для "путей к покупкам" и брендов.

### POST - получение фрагмента каталога  [POST]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "filters": [
                    42,  // эксклюзив
                    41   // новинка
                ]
            }
            
    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "filters": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список над-фильтров: пути к покупкам, бренды",
                        "items": {
                            "type": "integer",
                            "minimum": 1,
                            "description": "идентификатор фильтра",
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["filters"]
            }

+ Response 200 (application/json)
    + Body

            {
                "catalog": [
                    {
                        "id": 1,
                        "name": "Ароматы",
                        "subs": [
                            {
                                "id": 101,
                                "name": "Женские ароматы",
                                "subs": []
                            },
                            {
                                "id": 102,
                                "name": "Мужские ароматы",
                                "subs": []
                            },
                            {
                                "id": 104,
                                "name": "Наборы",
                                "subs": []
                            }
                      ]
                    },
                    {
                        "id": 2,
                        "name": "Макияж",
                        "subs": [
                            {
                                "id": 201,
                                "name": "Тон",
                                "subs": [
                                    {
                                        "id": 20101,
                                        "name": "База/Основа"
                                    },
                                    {
                                        "id": 20102,
                                        "name": "Тональный крем"
                                    },
                                    {
                                        "id": 20103,
                                        "name": "Компактная пудра"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
    
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "catalog": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "сокращенный каталог категорий, актуальный для данного списка товарных групп",
                        "items": {
                            "type": "object",
                            "description": "категория каталога 1 уровня",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор категории"
                                },
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название категории"
                                },
                                "subs": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список подкатегорий второго уровня",
                                    "items": {
                                        "type": "object",
                                        "description": "подкатегория второго уровня",
                                        "properties": {
                                           "id": {
                                                "type": "integer",
                                                "description": "идентификатор подкатегории"
                                            },
                                            "name": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 128,
                                                "description": "название подкатегории"
                                            },
                                            "subs": {
                                                "type": "array",
                                                "uniqueItems": true,
                                                "description": "список подкатегорий третьего уровня",
                                                "items": {
                                                    "type": "object",
                                                    "description": "подкатегория третьего уровня",
                                                    "properties": {
                                                        "id": {
                                                            "type": "integer",
                                                            "description": "идентификатор подкатегории"
                                                        },
                                                        "name": {
                                                            "type": "string",
                                                            "minLength": 1,
                                                            "maxLength": 128,
                                                            "description": "название подкатегории"
                                                        }
                                                    },
                                                    "additionalProperties": false,
                                                    "required": ["id", "name"]
                                                }
                                            } 
                                        },
                                        "additionalProperties": false,
                                        "required": ["id", "name"]
                                    }
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "name", "subs"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["catalog"]
            }

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 23,
                        "message": "Не определен один из фильров или одна из категорий"
                    }
                ]
            }

+ Response 401 (application/json)

## /catalog/groups
Поиск по набору категорий каталога и фильтрам.
Возвращает описания групп товаров в соответствии с запросом страницами по 36 штук.
{?} - на текущем этапе реализации в списке **categories** можно передавать только один элемент: 
остальные не будут использоваться сервером при поиске.

### POST - получение списка групп товаров [POST]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "categories": [
                    101
                ],
                "filters": [
                    412,
                    41
                ],
                "minPrice": 1600,
                "maxPrice": 6200,
                "sort": "name",
                "page": 1 // необязательный параметр
            }
            
    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "categories": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список категорий",
                        "items": {
                            "type": "integer",
                            "minimum": 1,
                            "maximum": 2147483647,
                            "description": "идентификатор категории",
                        }
                    },
                    "filters": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список фильтров",
                        "items": {
                            "type": "integer",
                            "minimum": 1,
                            "maximum": 2147483647,
                            "description": "идентификатор фильтра",
                        }
                    },
                    "minPrice": {
                        "type": "integer",
                        "minimum": 0,
                        "maximum": 2147483647,
                        "description": "минимальная цена"
                    },
                    "maxPrice": {
                        "type": "integer",
                        "minimum": 1,
                        "maximum": 2147483647,
                        "description": "максимальная цена"
                    },
                    "sort": {
                        "type": "string",
                        "enum": ["name", "cost_desc", "cost_asc", "brand"],
                        "description": "тип сортировки"
                    },
                    "page": {
                        "type": "integer",
                        "minimum": 1,
                        "maximum": 500,
                        "description": "номер страницы результатов (по 36 товарных групп на странице)"
                    }
                },
                "additionalProperties": false,
                "required": ["categories", "sort"]
            }

+ Response 200 (application/json)
    + Body

            {
                "groups_count": 145,
                "sort": "name",
                "page": 1,
                "groups": [
                    {
                        "id": 1,
                        "brand": "CLINIQUE",
                        "name": "Chubby Stick Baby Tint Увлажняющая помада-бальзам для губ",
                        "pictures": [
                            {
                                "width": 156,
                                "height": 156,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_5/115760/pre/156_156sb.jpg"
                            },
                            {
                                "width": 257,
                                "height": 257,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_5/115760/pre/257_257sb.jpg"
                            }
                        ],
                        "price": {
                            "type": 1,
                            "min": 75,
                            "max": 100,
                            "not_available": "temp"
                        },
                        "products": {
                            "count": 62,
                            "name": "оттенка"
                        },
                        "filters": [
                            412, // "15ml"
                            41 // "clinique"
                        ],
                        "has_gift": true
                    }
                ],
                "minPrice": 1600,
                "maxPrice": 6200,
                "filters": [
                    {
                        "group": "brand",
                        "name": "БРЕНДЫ",
                        "variants": [
                            {
                                "id": 33, // "acqua_di_parma"
                                "name": "ACQUA DI PARMA",
                                "count": 4
                            },
                            {
                                "id": 43, // "ainhoa"
                                "name": "AINHOA",
                                "count": 2
                            },
                            {
                                "id": 56, // "clinique"
                                "name": "CLINIQUE",
                                "count": 1
                            }
                        ]
                    },
                    {
                        "group": "volume",
                        "name": "ОБЪЁМ",
                        "variants": [
                            {
                                "value": 2, // "15ml"
                                "name": "15мл",
                                "count": 1
                            },
                            {
                                "value": 3, // "30ml"
                                "name": "30мл",
                                "count": 12
                            }
                        ]
                    }
                ]
            }
    
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "groups_count": {
                        "type": "integer",
                        "minimum": 1,
                        "description": "общее количество товарных групп в результатах поиска"
                    },
                    "sort": {
                        "type": "string",
                        "enum": ["name", "cost_desc", "cost_asc", "brand"],
                        "description": "тип сортировки"
                    },
                    "page": {
                        "type": "integer",
                        "minimum": 1,
                        "description": "номер страницы результатов (по 36 товарных групп на странице)"
                    },
                    "groups": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список товарных групп в данной категории",
                        "items": {
                            "type": "object",
                            "description": "группа",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор товарной группы"
                                },
                                "brand": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название бренда"
                                },
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название товарной группы"
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "price": {
                                    "type": "object",
                                    "description": "описание цены",
                                    "properties": {
                                        "type": {
                                            "type": "integer",
                                            "enum": [1, 2, 3],
                                            "description": "тип цены на товар: 1 - диапазон цен, возможна скидка по карте, 2 - фиксированная скидка, 3 - фиксированная цена"
                                        },
                                        "min": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "минимальная цена"
                                        },
                                        "max": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "максимальная цена"
                                        },
                                        "not_available": {
                                            "type": "string",
                                            "enum": ["temp", "perm"],
                                            "description": "'временно нет в наличии' или 'нет в продаже', если параметр не определен, то товар доступен для покупки"
                                        }
                                    },
                                    "additionalProperties": false,
                                    "required": ["type", "min", "max"]
                                },
                                "products": {
                                    "type": "object",
                                    "description": "количество вариантов в товарной группе",
                                    "properties": {
                                        "count": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "1 - всего один вариант, >1 - много вариантов (оттенков, объемов, аксессуаров) "
                                        },
                                        "name": {
                                            "type": "string",
                                            "description": "типов объекта по-русски: (62) оттенка, (11) оттенков, (21) объем"
                                        }
                                    },
                                    "additionalProperties": false,
                                    "required": ["count", "name"]
                                },
                                "filters": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список фильтров, в которых есть эта товарная группа",
                                    "items": {
                                        "type": "integer",
                                        "minimum": 1,
                                        "description": "идентификатор фильтра",
                                    }
                                },
                                "has_gift": {
                                    "type": "boolean",
                                    "description": "есть ли подарок к этой товарной группе"
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "brand", "name", "pictures", "price", "products", "filters", "has_gift"]
                        }
                    },
                    "minPrice": {
                        "type": "integer",
                        "minimum": 0,
                        "description": "минимальная цена"
                    },
                    "maxPrice": {
                        "type": "integer",
                        "minimum": 0,
                        "description": "максимальная цена"
                    },
                    "filters": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список групп фильтров, актуальных для данного списка товарных групп",
                        "items": {
                            "type": "object",
                            "description": "группа фильтров",
                            "properties": {
                                "group": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 32,
                                    "description": "идентификатор группы (латинские строчные символы, цифры и знак _)"
                                },
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название группы"
                                },
                                "variants": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список фильтров внутри группы",
                                    "items": {
                                        "type": "object",
                                        "description": "фильтр",
                                        "properties": {
                                           "id": {
                                                "type": "integer",
                                                "minimum": 1,
                                                "description": "идентификатор фильтра"
                                            },
                                            "name": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 128,
                                                "description": "отображение фильтра"
                                            },
                                            "count": {
                                                "type": "integer",
                                                "minimum": 1,
                                                "description": "количество товарных групп с этим фильтром"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["id", "name", "count"]
                                    }
                                }
                            },
                            "additionalProperties": false,
                            "required": ["group", "name", "variants"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["groups_count", "sort", "page", "groups", "minPrice", "maxPrice", "filters"]
            }

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 23,
                        "message": "Не определен один из фильров или одна из категорий"
                    },
                    {
                        "code": 32,
                        "message": "Минимальная цена больше максимальной"
                    },
                    {
                        "code": 35,
                        "message": "Запрошенной страницы разультатов не существует"
                    }
                ]
            }

+ Response 401 (application/json)

## /catalog/groups/fastsearch
{?} - метод запланирован на будущее, на текущем этапе разработки приложения не планируется его использовать.
Быстрый поиск вариантов поисковых фраз в момент набора фразы в поиске.
Возвращает первые 10 результатов в порядке релевантности.

### POST - получение списка подсказок для поиска [POST]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "query": "лак"
            }
            
    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "query": {
                        "type": "string",
                        "minLength": 2,
                        "description": "подстрока для поиска подсказок"
                    }
                },
                "additionalProperties": false,
                "required": ["query"]
            }

+ Response 200 (application/json)
    + Body

            {
                "tips": [
                    "CARTIER",
                    "CARITA"
                ]
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "tips": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список подсказок",
                        "items": {
                            "type": "string",
                            "minLength": 1,
                            "maxLength": 64,
                            "description": "подсказка"
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["tips"]
            }

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    }
                ]
            }

+ Response 401 (application/json)

## /catalog/groups/search
Поиск по каталогу по поисковой фразе и фильтрам. 
Возвращает описания групп товаров страницами по 36 групп товаров на странице.

### POST - получение списка групп товаров [POST]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "query": "лак",
                "filters": [
                    412,
                    41
                ],
                "minPrice": 1600,
                "maxPrice": 6200,
                "sort": "name",
                "page": 1
            }
            
    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "query": {
                        "type": "string",
                        "minLength": 3,
                        "maxLength": 255,
                        "description": "поисковый запрос"
                    },
                    "filters": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список фильтров",
                        "items": {
                            "type": "integer",
                            "minimum": 1,
                            "maximum": 2147483647,
                            "description": "идентификатор фильтра",
                        }
                    },
                    "minPrice": {
                        "type": "integer",
                        "minimum": 0,
                        "maximum": 2147483647,
                        "description": "минимальная цена"
                    },
                    "maxPrice": {
                        "type": "integer",
                        "minimum": 1,
                        "maximum": 2147483647,
                        "description": "максимальная цена"
                    },
                    "sort": {
                        "type": "string",
                        "enum": ["name", "cost_desc", "cost_asc", "brand"],
                        "description": "тип сортировки"
                    },
                    "page": {
                        "type": "integer",
                        "minimum": 1,
                        "maximum": 500,
                        "description": "номер страницы результатов (по 36 товарных групп на странице)"
                    }
                },
                "additionalProperties": false,
                "required": ["query", "sort"]
            }

+ Response 200 (application/json)
    + Body

            {
                "groups_count": 145,
                "sort": "name",
                "page": 1,
                "groups": [
                    {
                        "id": 1,
                        "brand": "CLINIQUE",
                        "name": "Chubby Stick Baby Tint Увлажняющая помада-бальзам для губ",
                        "pictures": [
                            {
                                "width": 156,
                                "height": 156,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_5/115760/pre/156_156sb.jpg"
                            },
                            {
                                "width": 257,
                                "height": 257,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_5/115760/pre/257_257sb.jpg"
                            }
                        ],
                        "price": {
                            "type": 1,
                            "min": 75,
                            "max": 100,
                            "not_available": "temp"
                        },
                        "products": {
                            "count": 62,
                            "name": "оттенка"
                        },
                        "filters": [
                            412, // "15ml"
                            41 // "clinique"
                        ],
                        "has_gift": true
                    }
                ],
                "minPrice": 1600,
                "maxPrice": 6200,
                "filters": [
                    {
                        "group": "brand",
                        "name": "БРЕНДЫ",
                        "variants": [
                            {
                                "id": 33, // "acqua_di_parma"
                                "name": "ACQUA DI PARMA",
                                "count": 4
                            },
                            {
                                "id": 43, // "ainhoa"
                                "name": "AINHOA",
                                "count": 2
                            },
                            {
                                "id": 56, // "clinique"
                                "name": "CLINIQUE",
                                "count": 1
                            }
                        ]
                    },
                    {
                        "group": "volume",
                        "name": "ОБЪЁМ",
                        "variants": [
                            {
                                "value": 2, // "15ml"
                                "name": "15мл",
                                "count": 1
                            },
                            {
                                "value": 3, // "30ml"
                                "name": "30мл",
                                "count": 12
                            }
                        ]
                    }
                ]
            }
    
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "groups_count": {
                        "type": "integer",
                        "minimum": 1,
                        "description": "общее количество товарных групп в результатах поиска"
                    },
                    "sort": {
                        "type": "string",
                        "enum": ["name", "cost_desc", "cost_asc", "brand"],
                        "description": "тип сортировки"
                    },
                    "page": {
                        "type": "integer",
                        "minimum": 1,
                        "description": "номер страницы результатов (по 36 товарных групп на странице)"
                    },
                    "groups": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список товарных групп в данной категории",
                        "items": {
                            "type": "object",
                            "description": "группа",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор товарной группы"
                                },
                                "brand": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название бренда"
                                },
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название товарной группы"
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "price": {
                                    "type": "object",
                                    "description": "описание цены",
                                    "properties": {
                                        "type": {
                                            "type": "integer",
                                            "enum": [1, 2, 3],
                                            "description": "тип цены на товар: 1 - диапазон цен, возможна скидка по карте, 2 - фиксированная скидка, 3 - фиксированная цена"
                                        },
                                        "min": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "минимальная цена"
                                        },
                                        "max": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "максимальная цена"
                                        },
                                        "not_available": {
                                            "type": "string",
                                            "enum": ["temp", "perm"],
                                            "description": "'временно нет в наличии' или 'нет в продаже', если параметр не определен, то товар доступен для покупки"
                                        }
                                    },
                                    "additionalProperties": false,
                                    "required": ["type", "min", "max"]
                                },
                                "products": {
                                    "type": "object",
                                    "description": "количество вариантов в товарной группе",
                                    "properties": {
                                        "count": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "1 - всего один вариант, >1 - много вариантов (оттенков, объемов, аксессуаров) "
                                        },
                                        "name": {
                                            "type": "string",
                                            "description": "типов объекта по-русски: (62) оттенка, (11) оттенков, (21) объем"
                                        }
                                    },
                                    "additionalProperties": false,
                                    "required": ["count", "name"]
                                },
                                "filters": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список фильтров, в которых есть эта товарная группа",
                                    "items": {
                                        "type": "integer",
                                        "minimum": 1,
                                        "description": "идентификатор фильтра",
                                    }
                                },
                                "has_gift": {
                                    "type": "boolean",
                                    "description": "есть ли подарок к этой товарной группе"
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "brand", "name", "pictures", "price", "products", "filters", "has_gift"]
                        }
                    },
                    "minPrice": {
                        "type": "integer",
                        "minimum": 0,
                        "description": "минимальная цена"
                    },
                    "maxPrice": {
                        "type": "integer",
                        "minimum": 0,
                        "description": "максимальная цена"
                    },
                    "filters": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список групп фильтров, актуальных для данного списка товарных групп",
                        "items": {
                            "type": "object",
                            "description": "группа фильтров",
                            "properties": {
                                "group": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 32,
                                    "description": "идентификатор группы (латинские строчные символы, цифры и знак _)"
                                },
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название группы"
                                },
                                "variants": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список фильтров внутри группы",
                                    "items": {
                                        "type": "object",
                                        "description": "фильтр",
                                        "properties": {
                                           "id": {
                                                "type": "integer",
                                                "minimum": 1,
                                                "description": "идентификатор фильтра"
                                            },
                                            "name": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 128,
                                                "description": "отображение фильтра"
                                            },
                                            "count": {
                                                "type": "integer",
                                                "minimum": 1,
                                                "description": "количество товарных групп с этим фильтром"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["id", "name", "count"]
                                    }
                                }
                            },
                            "additionalProperties": false,
                            "required": ["group", "name", "variants"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["groups_count", "sort", "page", "groups", "minPrice", "maxPrice", "filters"]
            }

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 23,
                        "message": "Не определен один из фильров или одна из категорий"
                    },
                    {
                        "code": 32,
                        "message": "Минимальная цена больше максимальной"
                    },
                    {
                        "code": 35,
                        "message": "Запрошенной страницы разультатов не существует"
                    }
                ]
            }

+ Response 401 (application/json)

## /catalog/groups/{id}

### GET - получение списка товаров в группе [GET]
+ Parameters
    + id: 16913 (required, number) - ID товарной группы

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==
    
    
+ Response 200 (application/json)
    + Body

            {                
                "id": 88953,
                "brand": "LANCOME",
                "name": "Visionnaire Yeux Уход для глаз",
                "pictures": [
                    {
                        "width": 156,
                        "height": 156,
                        "url": "http://static.iledebeaute.ru/files/images/tag/part_5/115760/pre/156_156sb.jpg"
                    },
                    {
                        "width": 500,
                        "height": 500,
                        "url": "http://static.iledebeaute.ru/files/images/tag/part_5/115760/pre/257_257sb.jpg"
                    }
                ],
                "products": [
                    {
                        "id": 10000,
                        "articul": "L9171300",
                        "extra": "Poppin' Poppy",
                        "volume": "5мл",
                        "pictures": [
                            {
                                "width": 500,
                                "height": 500,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_4/97608/pre/112_68sb.jpg"
                            }
                        ],
                        "price": {
                            "type": 1,
                            "min": 75,
                            "max": 100,
                            "not_available": "temp"
                        },
                        "filters": [
                            1, // "new"
                            2, // "online_only"
                        ]
                    }
                ],
                "description": 
                    "Универсальный уход-корректор для кожи вокруг глаз Visionnaire Yeux, обогащенный действенной молекулой LR 2412, борющейся с темными кругами, морщинками и неровным тоном кожи вокруг глаз.\n\nРезультат: день за днем тон кожи вокруг глаз становится более ровным, темные круги и мимические морщинки кажутся менее заметными, кожа вокруг глаз выглядит сияющей.",
                "composition": "– 72 оттенка теней – 32 блеска для губ",
                "instruction": "Для использования в качестве дневного и/или ночного средства ухода. Наносить ежедневно, утром и вечером.", 
                "gift": {
                    "id": 5235,
                    "articul": "L5814600LRL",
                    "brand": "LANCOME",
                    "name": "Миниатюра Advanced Genigique Light-Pearl",
                    "pictures": [
                        {
                            "width": 500,
                            "height": 500,
                            "url": "http://static.iledebeaute.ru/files/images/tag/part_4/97608/pre/112_68sb.jpg"
                        }
                    ],
                    "text": "При любой покупке продуктов ухода Lancome вы получаете в подарок миниатюру сыворотки Advanced Genigique Light-Pearl, 5 мл."
                },
                "sharing_url": "http://iledebeaute.ru/shop/sets/fragrance-woman/nabor_ange_ou_demon_le_secret;2hbk/"
            }

    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "description": "товар",
                "properties": {
                    "id": {
                        "type": "integer",
                        "description": "идентификатор товара"
                    },
                    "brand": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 128,
                        "description": "название бренда товара"
                    },
                    "name": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 128,
                        "description": "название товара"
                    },
                    "pictures": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список картинок",
                        "items": {
                            "type": "object",
                            "description": "описание картинки",
                            "properties": {
                                "width": {
                                    "type": "integer",
                                    "minimum": 10,
                                    "maximum": 5000,
                                    "description": "ширина картинки"
                                },
                                "height": {
                                    "type": "integer",
                                    "minimum": 10,
                                    "maximum": 5000,
                                    "description": "высота картинки"
                                },
                                "url": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 1024,
                                    "description": "пусть к картинке"
                                }
                            },
                            "additionalProperties": false,
                            "required": ["width","height","url"]
                        }
                    },
                    "products": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "товары в группе",
                        "items": {
                            "type": "object",
                            "properties": {
                               "id": {
                                    "type": "integer",
                                    "description": "идентификатор товара"
                                },
                                "articul": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 16,
                                    "description": "артикул"
                                },
                                "extra": {
                                    "type": "string",
                                    "minLength": 0,
                                    "maxLength": 128,
                                    "description": "строка с доп. текстом: название оттенка, аксессуара и прочее"
                                },
                                "volume": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 16,
                                    "description": "объем"
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "price": {
                                    "type": "object",
                                    "description": "описание цены",
                                    "properties": {
                                        "type": {
                                            "type": "integer",
                                            "enum": [1, 2, 3],
                                            "description": "тип цены на товар: 1 - диапазон цен, возможна скидка по карте, 2 - фиксированная скидка, 3 - фиксированная цена"
                                        },
                                        "min": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "минимальная цена"
                                        },
                                        "max": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "максимальная цена"
                                        },
                                        "not_available": {
                                            "type": "string",
                                            "enum": ["temp", "perm"],
                                            "description": "'временно нет в наличии' или 'нет в продаже', если параметр не определен, то товар доступен для покупки"
                                        }
                                    },
                                    "additionalProperties": false,
                                    "required": ["type", "min", "max"]
                                },
                                "filters": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список фильтров, в которых есть этот товар (например: новинка, эксклюзив)",
                                    "items": {
                                        "type": "integer",
                                        "minimum": 1,
                                        "description": "идентификатор фильтра",
                                    }
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "brand", "name", "articul", "extra", "pictures", "price", "filters"]
                        }
                    },
                    "description": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 4096,
                        "description": "описание"
                    },
                    "composition": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 4096,
                        "description": "состав набора (может быть пустой строкой - тогда показывать этот блок не нужно)"
                    },
                    "instruction": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 4096,
                        "description": "инструкция (может быть пустой строкой - тогда показывать этот блок не нужно)"
                    },
                    "gift": {
                        "type": "object",
                        "description": "товар",
                        "properties": {
                            "id": {
                                "type": "integer",
                                "minimum": 1,
                                "description": "идентификатор подарка"
                            },
                            "articul": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 16,
                                "description": "артикул подарка"
                            },
                            "brand": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 128,
                                "description": "название бренда товара"
                            },
                            "name": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 128,
                                "description": "название товара"
                            },
                            "pictures": {
                                "type": "array",
                                "uniqueItems": true,
                                "description": "список картинок",
                                "items": {
                                    "type": "object",
                                    "description": "описание картинки",
                                    "properties": {
                                        "width": {
                                            "type": "integer",
                                            "minimum": 10,
                                            "maximum": 5000,
                                            "description": "ширина картинки"
                                        },
                                        "height": {
                                            "type": "integer",
                                            "minimum": 10,
                                            "maximum": 5000,
                                            "description": "высота картинки"
                                        },
                                        "url": {
                                            "type": "string",
                                            "minLength": 1,
                                            "maxLength": 1024,
                                            "description": "пусть к картинке"
                                        }
                                    },
                                    "additionalProperties": false,
                                    "required": ["width", "height", "url"]
                                }
                            },
                            "text": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 4096,
                                "description": "описание акции, по которой дается подарок"
                            }
                        },
                        "additionalProperties": false,
                        "required":["id", "articul", "brand", "name", "pictures", "text"]
                    },
                    "sharing_url": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 1024,
                        "format": "uri",
                        "description": "ссылка на страницу товара для шаринга"
                    }
                },
                "additionalProperties": false,
                "required":["id", "brand", "name", "pictures", "products", "description", "composition", "instruction"]

            }

+ Response 401 (application/json)

+ Response 404 (application/json)

# Group Shops

## /shops/cities

### GET - получение списка городов [GET]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==
    
+ Response 200 (application/json)
    + Body

            {
                "cities": [
                    {
                        "id": 1,
                        "name": "Москва",
                        "longitude": "59.921621",
                        "latitude": "30.467361"
                    },
                    {
                        "id": 2,
                        "name": "Санкт-Петербург",
                        "longitude": "59.921621",
                        "latitude": "30.467361"
                    }
                ]
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "cities": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список городов",
                        "items": {
                            "type": "object",
                            "description": "город",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор города"
                                },
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название города"
                                },
                                "longitude": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 16,
                                    "description": "долгота"
                                },
                                "latitude": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 16,
                                    "description": "широта"
                                } 
                            },
                            "additionalProperties": false,
                            "required": ["id", "name", "longitude", "latitude"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["cities"]
            }

+ Response 401 (application/json)

## /shops/cities/{id}

### GET - получение списка магазинов в городе [GET]
+ Parameters
    + id: 123 (required, number) - ID города

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==
    
+ Response 200 (application/json)
    + Body

            {
                "shops": [
                    {
                        "id": 1,
                        "address": {
                            "metro": [
                                "Пионерская"
                            ],
                            "mall": "ТРК \"Сити Молл\"",
                            "street": "Коломяжский проспект, д. 17"
                        },
                        "longitude": "59.921621",
                        "latitude": "30.467361",
                        "worktime": {
                            "default": {
                                "opening": 1000,
                                "closing": 2200
                            }
                        },
                        "phones": [
                            {
                                "name": "Телефон магазина",
                                "number": "78123180681"
                            },
                            {
                                "name": "Круглосуточная горячая линия",
                                "number": "88002001700"
                            }
                        ]
                    },
                    {
                        "id": 2,
                        "address": {
                            "metro": [
                                "Площадь Восстания",
                                "Маяковская"
                            ],
                            "mall": "ТЦ \"Галерея\"",
                            "street": "Лиговский проспект, д. 30"
                        },
                        "longitude": "59.921621",
                        "latitude": "30.467361",
                        "worktime": {
                            "default": {
                                "opening": 1000,
                                "closing": 2300
                            },
                            "other": [
                                {
                                    "day": 5,
                                    "opening": 900,
                                    "closing": 2200
                                },
                                {
                                    "day": 6,
                                    "opening": 900,
                                    "closing": 2200
                                }
                            ]
                        },
                        "phones": [
                            {
                                "name": "Телефон магазина",
                                "number": "78123180681"
                            },
                            {
                                "name": "Круглосуточная горячая линия",
                                "number": "88002001700"
                            }
                        ]
                    }
                ]
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "shops": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список магазинов",
                        "items": {
                            "type": "object",
                            "description": "магазин",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор магазина"
                                },
                                "address": {
                                    "type": "object",
                                    "description": "адрес магазина",
                                    "properties": {
                                        "metro": {
                                            "type": "array",
                                            "items": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 32,
                                                "description": "станция метро, возле которой расположен магазин",
                                            }
                                        },
                                        "mall": {
                                            "type": "string",
                                            "minLength": 1,
                                            "maxLength": 32,
                                            "description": "торговый центр"
                                        },
                                        "street": {
                                            "type": "string",
                                            "minLength": 1,
                                            "maxLength": 32,
                                            "description": "улица, дом"
                                        }
                                    },
                                    "additionalProperties": false,
                                    "required": ["street"]
                                },
                                "longitude": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 16,
                                    "description": "долгота"
                                },
                                "latitude": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 16,
                                    "description": "широта"
                                },
                                "worktime": {
                                    "type": "object",
                                    "description": "время работы магазина",
                                    "properties": {
                                        "default": {
                                            "type": "object",
                                            "description": "стандартное время работы магазина",
                                            "properties": {
                                                "opening": {
                                                    "type": "integer",
                                                    "description": "время открытия"
                                                },
                                                "closing": {
                                                    "type": "integer",
                                                    "description": "время закрытия"
                                                }
                                            },
                                            "additionalProperties": false,
                                            "required": ["opening", "closing"]
                                        },
                                        "other": {
                                            "type": "array",
                                            "description": "нестадартное время работы магазина",
                                            "items": {
                                                "type": "object",
                                                "properties": {
                                                    "day": {
                                                        "type": "integer",
                                                        "enum": [0, 1, 2, 3, 4, 5, 6],
                                                        "description": "номер дня"
                                                    },
                                                    "opening": {
                                                        "type": "integer",
                                                        "description": "время открытия"
                                                    },
                                                    "closing": {
                                                        "type": "integer",
                                                        "description": "время закрытия"
                                                    }
                                                },
                                                "additionalProperties": false,
                                                "required": ["day", "opening", "closing"]
                                            }
                                        }
                                    }
                                },
                                "phones": {
                                    "type": "array",
                                    "description": "номера телефонов",
                                    "items": {
                                        "type": "object",
                                        "properties": {
                                            "name": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 32,
                                                "description": "название"
                                            },
                                            "number": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 32,
                                                "description": "номер"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["name", "number"]
                                    }
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "address", "longitude", "latitude", "worktime", "phones"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["shops"]
            }   

+ Response 401 (application/json)

+ Response 404 (application/json)

## /shops/{id}

### GET - получение информации о магазине [GET]
+ Parameters
    + id: 123 (required, number) - ID магазина

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)
    + Body

            {
                "address": {
                    "metro": [
                        "Площадь Восстания",
                        "Маяковская"
                    ],
                    "mall": "ТЦ \"Галерея\"",
                    "street": "Лиговский проспект, д. 30"
                },
                "longitude": "59.921621",
                "latitude": "30.467361",
                "worktime": {
                    "default": {
                        "opening": 1000,
                        "closing": 2300
                    },
                    "other": [
                        {
                            "day": 5,
                            "opening": 900,
                            "closing": 2200
                        },
                        {
                            "day": 6,
                            "opening": 900,
                            "closing": 2200
                        }
                    ]
                },
                "phones": [
                    {
                        "name": "Телефон магазина",
                        "number": "78123180681"
                    },
                    {
                        "name": "Круглосуточная горячая линия",
                        "number": "88002001700"
                    }
                ],
                "brands": [
                    "ACQUA DI PARMA",
                    "AINHOA"
                ]
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "address": {
                        "type": "object",
                        "description": "адрес магазина",
                        "properties": {
                            "metro": {
                                "type": "array",
                                "items": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 32,
                                    "description": "станция метро, возле которой расположен магазин",
                                }
                            },
                            "mall": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 32,
                                "description": "торговый центр"
                            },
                            "street": {
                                "type": "string",
                                "minLength": 1,
                                "maxLength": 32,
                                "description": "улица, дом"
                            }
                        },
                        "additionalProperties": false,
                        "required": ["street"]
                    },
                    "longitude": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 16,
                        "description": "долгота"
                    },
                    "latitude": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 16,
                        "description": "широта"
                    },
                    "worktime": {
                        "type": "object",
                        "description": "время работы магазина",
                        "properties": {
                            "default": {
                                "type": "object",
                                "description": "стандартное время работы магазина",
                                "properties": {
                                    "opening": {
                                        "type": "integer",
                                        "description": "время открытия"
                                    },
                                    "closing": {
                                        "type": "integer",
                                        "description": "время закрытия"
                                    }
                                },
                                "additionalProperties": false,
                                "required": ["opening", "closing"]
                            },
                            "other": {
                                "type": "array",
                                "description": "нестадартное время работы магазина",
                                "items": {
                                    "type": "object",
                                    "properties": {
                                        "day": {
                                            "type": "integer",
                                            "enum": [0, 1, 2, 3, 4, 5, 6],
                                            "description": "номер дня"
                                        },
                                        "opening": {
                                            "type": "integer",
                                            "description": "время открытия"
                                        },
                                        "closing": {
                                            "type": "integer",
                                            "description": "время закрытия"
                                        }
                                    },
                                    "additionalProperties": false,
                                    "required": ["day", "opening", "closing"]
                                }
                            }
                        },
                        "additionalProperties": false,
                        "required": ["default"]
                    },
                    "phones": {
                        "type": "array",
                        "description": "номера телефонов",
                        "items": {
                            "type": "object",
                            "properties": {
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 32,
                                    "description": "название"
                                },
                                "number": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 32,
                                    "description": "номер"
                                }
                            },
                            "additionalProperties": false,
                            "required": ["name", "number"]
                        }
                    },
                    "brands": {
                        "type": "array",
                        "items": {
                            "type": "string",
                            "minLength": 1,
                            "maxLength": 32,
                            "description": "наименование бренда"
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["id", "address", "longitude", "latitude", "worktime", "phones"]
            }   

+ Response 401 (application/json)

+ Response 404 (application/json)

# Group Banners
## /banners/general
Метод будет обновлен после согласования прототипа главного экрана приложения

### GET - получение баннеров для главного экрана приложения [GET]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)
    + Body

            {
                "banners": [
                    {
                        "id": 103,
                        "pictures": [
                            {
                                "width": 500,
                                "height": 500,
                                "url": "http://iledebeaute.ru/@/project/fit/539x322_sale_jan16.jpg"
                            }
                        ],
                        "position": "top", // слайдер с баннерами в верхней части главного экрана каталога
                        "action": {
                            // см. описание Actions 
                        }
                    },
                    {
                        "id": 105,
                        "pictures": [
                            {
                                "width": 500,
                                "height": 500,
                                "url": "http://iledebeaute.ru/@/project/fit/tizer-im-abeille-190115.jpg"
                            }
                        ],
                        "position": "bottom", // баннеры в нижней части главного экрана каталога
                        "action": {
                            // см. описание Actions
                        }
                    },
                    {
                        "id": 115,
                        "pictures": [
                            {
                                "width": 500,
                                "height": 500,
                                "url": "http://iledebeaute.ru/@/project/fit/tizer-im-abeille-190115.jpg"
                            }
                        ],
                        "text": "Представляем новинку от Erborian!\nПитательное молочко Женьшень Только в Online-магазине ИЛЬ ДЕ БОТЭ!",
                        "position": "top", // слайдер с баннерами в верхней части главного экрана каталога
                        "action": {
                            // см. описание Actions
                        }
                    }
                ]
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "banners": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список баннеров",
                        "items": {
                            "type": "object",
                            "description": "баннер",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор баннера"
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "position": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 8,
                                    "enum": ["top", "bottom"],
                                    "description": "положение баннера"
                                },
                                "action": {
                                    // см. описание Actions
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "pictures", "position", "action"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["banners"]
            }       

+ Response 401 (application/json)

## /banners/catalog
{?} Пока используются только первый баннер в массиве только для категорий первого уровня.
Возможно расширение до листалки баннеров на каждом из мест. Но только для категорий первого уровня.

### GET - получение баннеров для показа внутри основного каталога [GET]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)

    + Body

            {
                "banners": [
                    {
                        "category_id": 1,
                        "id": 103,
                        "pictures": [
                            {
                                "width": 500,
                                "height": 500,
                                "url": "http://iledebeaute.ru/@/project/fit/539x322_sale_jan16.jpg"
                            }
                        ],
                        "action": {
                            // см. описание Actions 
                        }
                    },
                    {
                        "category_id": 101,
                        "id": 105,
                        "pictures": [
                            {
                                "width": 500,
                                "height": 500,
                                "url": "http://iledebeaute.ru/@/project/fit/tizer-im-abeille-190115.jpg"
                            }
                        ],
                        "action": {
                            // см. описание Actions
                        }
                    }
                ]
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "banners": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список баннеров",
                        "items": {
                            "type": "object",
                            "description": "баннер",
                            "properties": {
                                "category_id": {
                                    "type": "integer",
                                    "description": "идентификатор категории каталога"
                                },
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор баннера"
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "action": {
                                    // см. описание Actions
                                }
                            },
                            "additionalProperties": false,
                            "required": ["category_id", "id", "pictures", "action"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["banners"]
            }       

+ Response 401 (application/json)

# Group Brands
## /brands

### GET - получение списка брендов [GET]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)
    + Body

            {
                "brands": [
                    {
                        "id": 1,
                        "name": "ACQUA DI PARMA",
                        "filter_id": 1
                    },
                    {
                        "id": 2,
                        "name": "CALVIN KLEIN",
                        "filter_id": 2
                    },
                    {
                        "id": 3,
                        "name": "DARPHIN",
                        "filter_id": 3
                    }
                ]
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "brands": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список брендов",
                        "items": {
                            "type": "object",
                            "description": "бренд",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор бренда"
                                },
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 32,
                                    "description": "название бренда"
                                },
                                "filter_id": {
                                    "type": "integer",
                                    "description": "идентификатор фильра"
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "name", "filter_id"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["brands"]
            }   
    
+ Response 401 (application/json)

# Group Offers
## /offers
Первая акция на экране показывается в виде баннеров, в описании этой акции есть поля **banner_id** и **banner_pictures**.

### GET - получение списка акций [GET]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)
    + Body

            {
                "offers": [
                    {
                        "id": 1,
                        "pictures": [
                            {
                                "width": 150,
                                "height": 150,
                                "url": "http://static.iledebeaute.ru/files/images/pub/part_3/64001/pre/150_150.jpg"
                            }
                        ],
                        "header": "Миниатюра Génifique Yeux Light-Pearl в подарок от Lancôme",
                        "tags": [
                            1, // only mobile
                        ],
                        "text": "В Online-магазине ИЛЬ ДЕ БОТЭ с 1 по 24 января 2016 года включительно проходит беспрецедентная акция...",
                        "description": "В акции участвуют ведущие марки, эксклюзивы сети, а также подарочные наборы, ароматы, продукты...",
                        "action": {
                            // см. описание Actions
                        },
                        "banner_id": 32, // необязательный параметр
                        "banner_pictures": [
                            {
                                "width": 480,
                                "height": 200,
                                "url": "http://static.iledebeaute.ru/files/images/pub/part_3/64001/pre/480_200.jpg"
                            }
                        ]
                        "sharing_url": "http://iledebeaute.ru/shop/actions/2016/1/1/64161/#Распродажа_в_Online-магазине_ИЛЬ_ДЕ_БОТЭ!" // необязательный параметр
                    },
                    {
                        "id": 2,
                        "pictures": [
                            {
                                "width": 150,
                                "height": 150,
                                "url": "http://static.iledebeaute.ru/files/images/pub/part_3/64001/pre/150_150.jpg"
                            }
                        ],
                        "header": "Распродажа в Online-магазине ИЛЬ ДЕ БОТЭ!",
                        "tags": [
                            1, // only mobile
                        ],
                        "text": "В Online-магазине ИЛЬ ДЕ БОТЭ с 1 по 24 января 2016 года включительно проходит беспрецедентная акция...",
                        "description": "В акции участвуют ведущие марки, эксклюзивы сети, а также подарочные наборы, ароматы, продукты...",
                        "action": {
                            // см. описание Actions
                        }
                        "sharing_url": "http://iledebeaute.ru/shop/actions/2016/1/1/64161/#Распродажа_в_Online-магазине_ИЛЬ_ДЕ_БОТЭ!" // необязательный параметр
                    },
                    {
                        "id": 3,
                        "pictures": [
                            {
                                "width": 150,
                                "height": 150,
                                "url": "http://static.iledebeaute.ru/files/images/pub/part_3/64001/pre/150_150.jpg"
                            }
                        ],
                        "header": "Эксклюзивные подарки в Online-магазине ИЛЬ ДЕ БОТЭ в январе 2016 года!",
                        "text": "В Online-магазине ИЛЬ ДЕ БОТЭ с 1 по 24 января 2016 года включительно проходит беспрецедентная акция...",
                        "description": "В акции участвуют ведущие марки, эксклюзивы сети, а также подарочные наборы, ароматы, продукты...",
                        "action": {
                            // см. описание Actions
                        }
                        "sharing_url": "http://iledebeaute.ru/shop/actions/2016/1/1/64161/#Распродажа_в_Online-магазине_ИЛЬ_ДЕ_БОТЭ!" // необязательный параметр
                    }
                ]
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "offers": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список акций",
                        "items": {
                            "type": "object",
                            "description": "акция",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор акции"
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "header": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 512,
                                    "description": "заголовок акции"
                                },
                                "tags": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список тегов",
                                    "items": {
                                        "type": "integer",
                                        "minimum": 1,
                                        "description": "идентификатор тега"
                                    }
                                },
                                "text": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 256,
                                    "description": "краткое описание акции"
                                },
                                "description": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 1024,
                                    "description": "описание акции"
                                },
                                "action": {
                                    // см. описание Actions
                                },
                                "banner_id": {
                                    "type": "integer",
                                    "minimum": 1,
                                    "description": "id баннера"
                                },
                                "banner_pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок для показа баннера",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "sharing_url": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 1024,
                                    "format": "uri",
                                    "description": "ссылка на акцию для шаринга"
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "pictures", "header", "tags", "text", "action"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["offers"]
            }       
    
+ Response 401 (application/json)

# Group Collections
## /collections

### GET - получение списка коллекций [GET]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)
    + Body

            {
                "collections": [
                    {
                        "id": 1,
                        "brand": "ANNY",
                        "pictures": [
                            {
                                "width": 347,
                                "height": 169,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_5/117354/pre/347_169.jpg"
                            }
                        ],
                        "header": "ANNY - Коллекция лаков Luxury Mountain Resort"
                    },
                    {
                        "id": 2,
                        "brand": "DOLCE&GABBANA",
                        "pictures": [
                            {
                                "width": 347,
                                "height": 169,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_5/116803/pre/347_169.jpg"
                            }
                        ],
                        "header": "DOLCE&GABBANA - Рождественская коллекция The Essence of Holiday 2015"
                    }
                ]
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "collections": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список коллекций",
                        "items": {
                            "type": "object",
                            "description": "коллекция",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор коллекции"
                                },
                                "brand": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название бренда"
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "header": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 512,
                                    "description": "заголовок коллекции"
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "brand", "pictures", "header"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["collections"]
            }       

+ Response 401 (application/json)

## /collections/{id}

### GET - получение данных конкретной коллекции [GET]
+ Parameters
    + id: 110739 (required, number) - ID коллекции

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)
    + Body

            { 
                "id": 1,
                "pictures": [
                    {
                        "width": 257,
                        "height": 257,
                        "url": "http://static.iledebeaute.ru/files/images/tag/part_5/117354/pre/257_257sb.jpg"
                    }                    
                ],
                "brand": "Dolce&Gabbana",
                "header": "DOLCE&GABBANA - Рождественская коллекция The Essence of Holiday 2015",
                "description": "Уютные дни в горном шале: Аспен идеально подходит как для спокойного отдыха,...",
                "products": [
                    {
                        "id": 10000,
                        "name": "THE ONE FEMALE ESSENCE Парфюмерная вода",
                        "articul": "L9171300",
                        "extra": "Poppin' Poppy",
                        "volume": "5мл",
                        "pictures": [
                            {
                                "width": 500,
                                "height": 500,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_4/97608/pre/112_68sb.jpg"
                            }
                        ],
                        "price": {
                            "type": 1,
                            "min": 75,
                            "max": 100,
                            "not_available": "temp"
                        }
                    }
                ]
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "description": "идентификатор коллекции"
                    },
                    "pictures": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список картинок",
                        "items": {
                            "type": "object",
                            "description": "описание картинки",
                            "properties": {
                                "width": {
                                    "type": "integer",
                                    "minimum": 10,
                                    "maximum": 5000,
                                    "description": "ширина картинки"
                                },
                                "height": {
                                    "type": "integer",
                                    "minimum": 10,
                                    "maximum": 5000,
                                    "description": "высота картинки"
                                },
                                "url": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 1024,
                                    "description": "пусть к картинке"
                                }
                            },
                            "additionalProperties": false,
                            "required": ["width", "height", "url"]
                        }
                    },
                    "header": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 512,
                        "description": "заголовок коллекции"
                    },
                    "description": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 1024,
                        "description": "описание"
                    },
                    "products": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "товары в группе",
                        "items": {
                            "type": "object",
                            "properties": {
                               "id": {
                                    "type": "integer",
                                    "description": "идентификатор товара"
                                },
                                "articul": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 16,
                                    "description": "артикул"
                                },
                                "extra": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "строка с доп. текстом: название оттенка, объем и прочее"
                                },
                                "volume": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 16,
                                    "description": "объем"
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "price": {
                                    "type": "object",
                                    "description": "описание цены",
                                    "properties": {
                                        "type": {
                                            "type": "integer",
                                            "enum": [1, 2, 3],
                                            "description": "тип цены на товар: 1 - диапазон цен, возможна скидка по карте, 2 - фиксированная скидка, 3 - фиксированная цена"
                                        },
                                        "min": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "минимальная цена"
                                        },
                                        "max": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "максимальная цена"
                                        },
                                        "not_available": {
                                            "type": "string",
                                            "enum": ["temp", "perm"],
                                            "description": "'временно нет в наличии' или 'нет в продаже', если параметр не определен, то товар доступен для покупки"
                                        }
                                    },
                                    "additionalProperties": false,
                                    "required": ["type", "min", "max"]
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "articul", "extra", "pictures", "price"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["id", "pictures", "header", "description", "products"]
            } 

+ Response 401 (application/json)

+ Response 404 (application/json)

# Group Card
## /card

### GET - получение данных привязанной карты [GET]

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)
    + Body

            {
                "type": "club", // клубная
                "number": "2765000509654",
                "percent": 15,
                "summ": 10000,
                "increase_percent": 20,
                "increase_summ": 20000
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "type": {
                        "type": "string",
                        "enum": ["virtual", "student", "club", "vip"],
                        "description": "тип карты"
                    },
                    "number": {
                        "type": "string",
                        "minLength": 13,
                        "maxLength": 13,
                        "description": "номер карты"
                    },
                    "percent": {
                        "type": "number",
                        "minimum": 10,
                        "maximum": 25,
                        "description": "процент скидки"
                    },
                    "summ": {
                        "type": "number",
                        "minimum": 0,
                        "description": "сумма накоплений"
                    },
                    "increase_percent": {
                        "type": "number",
                        "minimum": 10,
                        "maximum": 25,
                        "description": "следующий процент скидки"
                    },
                    "increase_summ": {
                        "type": "number",
                        "minimum": 0,
                        "description": "сумма, необходимая до повышения процента скидки"
                    }                    
                },
                "additionalProperties": false,
                "required": ["type", "number", "percent", "summ"]
            } 
    
+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 38,
                        "message": "Нет привязанной карты"
                    }
                ]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

### POST - оформление карты [POST]

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "first_name": "Иван",
                "last_name": "Иванов",
                "sex": 1,
                "city": "Омск",
                "dob": "1990-09-22"
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "first_name": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "description": "имя"
                    },
                    "last_name": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "description": "фамилия"
                    },
                    "sex": {
                        "type": "number",
                        "enum": [0, 1, 2],
                        "description": "пол (0 - не выбран, 1 - женский, 2 - мужской)"
                    },
                    "city": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "description": "город"
                    },
                    "dob": {
                        "type": "string",
                        "format": "date",
                        "description": "дата рождения (YYYY-MM-DD)"
                    }
                },
                "additionalProperties": false,
                "required": ["first_name", "last_name", "sex", "city", "dob"]
            }

+ Response 200 (application/json)
    + Body

            {
                "type": "club", // клубная
                "number": "2765000509654",
                "percent": 15,
                "summ": 10000,
                "increase_percent": 20,
                "increase_summ": 20000
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "type": {
                        "type": "string",
                        "enum": ["virtual"],
                        "description": "тип карты"
                    },
                    "number": {
                        "type": "string",
                        "minLength": 13,
                        "maxLength": 13,
                        "description": "номер карты"
                    },
                    "percent": {
                        "type": "number",
                        "minimum": 10,
                        "maximum": 25,
                        "description": "процент скидки"
                    },
                    "summ": {
                        "type": "number",
                        "minimum": 0,
                        "description": "сумма накоплений"
                    },
                    "increase_percent": {
                        "type": "number",
                        "minimum": 10,
                        "maximum": 25,
                        "description": "следующий процент скидки"
                    },
                    "increase_summ": {
                        "type": "number",
                        "minimum": 0,
                        "description": "сумма, необходимая до повышения процента скидки"
                    }                    
                },
                "additionalProperties": false,
                "required": ["type", "number", "percent", "summ"]
            } 

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 39,
                        "message": "К аккаунту уже привязана карта"
                    }
                ]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

## /card/{id}

### POST - верификация анкеты пользователя и привязка карты [POST]
+ Parameters
    + id: 2765000509654 (required, number) - ID карты (13 цифр)

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "first_name": "Иван",
                "last_name": "Иванов",
                "sex": 1,
                "city": "Омск",
                "dob": "1990-09-22"
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "first_name": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "description": "имя"
                    },
                    "last_name": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "description": "фамилия"
                    },
                    "sex": {
                        "type": "number",
                        "enum": [0, 1, 2],
                        "description": "пол (0 - не выбран, 1 - женский, 2 - мужской)"
                    },
                    "city": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "description": "город"
                    },
                    "dob": {
                        "type": "string",
                        "format": "date",
                        "description": "дата рождения (YYYY-MM-DD)"
                    }
                },
                "additionalProperties": false,
                "required": ["first_name", "last_name", "sex", "city", "dob"]
            }

+ Response 200 (application/json)
    + Body

            {
                "type": "club", // клубная
                "number": "2765000509654",
                "percent": 15,
                "summ": 10000,
                "increase_percent": 20,
                "increase_summ": 20000
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "type": {
                        "type": "string",
                        "enum": ["virtual", "student", "club", "vip"],
                        "description": "тип карты"
                    },
                    "number": {
                        "type": "string",
                        "minLength": 13,
                        "maxLength": 13,
                        "description": "номер карты"
                    },
                    "percent": {
                        "type": "number",
                        "minimum": 10,
                        "maximum": 25,
                        "description": "процент скидки"
                    },
                    "summ": {
                        "type": "number",
                        "minimum": 0,
                        "description": "сумма накоплений"
                    },
                    "increase_percent": {
                        "type": "number",
                        "minimum": 10,
                        "maximum": 25,
                        "description": "следующий процент скидки"
                    },
                    "increase_summ": {
                        "type": "number",
                        "minimum": 0,
                        "description": "сумма, необходимая до повышения процента скидки"
                    }                    
                },
                "additionalProperties": false,
                "required": ["type", "number", "percent", "summ"]
            } 

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 21,
                        "message": "Введенные данные о карте не соответсвуют данным на сервере. Необходима дополнительная верификация"
                    },
                    {
                        "code": 37,
                        "message": "Карта уже привязана к другому аккаунту"
                    },
                    {
                        "code": 39,
                        "message": "К аккаунту уже привязана карта"
                    }
                ]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

## /card/{id}/purchases

### GET - получение списка последних покупок [GET]
+ Parameters
    + id: 2765000509654 (required, number) - ID карты (13 цифр)

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)
    + Body

            {
                "purchases": [
                    {
                        "id": 1,
                        "brand": "BENEFIT",
                        "name": "Beyond",
                        "extra": "Always Brindesmaid",
                        "volume": "5мл",
                        "count": 1,
                        "price": "11467.00",
                        "date": "2015-12-22"
                    },
                    {
                        "id": 2,
                        "brand": "BENEFIT",
                        "name": "Beyond",
                        "extra": "Always Brindesmaid",
                        "volume": "5мл",
                        "count": 1,
                        "price": "11467.10",
                        "date": "2015-12-22"
                    }
                ]
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "purchases": {
                        "type": "array",
                        "description": "список покупок, которые выбрал пользователь",
                        "items": {
                            "type": "object",
                            "properties": {
                                "id": {
                                    "type": "number",
                                    "description": "идентефикатор покупки"
                                },
                                "brand": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название бренда товара"
                                },
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название товара"
                                },
                                "extra": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "строка с доп. текстом: название оттенка, аксессуара и прочее"
                                },
                                "volume": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 16,
                                    "description": "объем"
                                },
                                "count": {
                                    "type": "integer",
                                    "minimum": 1,
                                    "description": "количество"
                                },
                                "price": {
                                    "type": "string",
                                    "description": "цена в формате 123124.20"
                                },
                                "date": {
                                    "type": "string",
                                    "format": "date",
                                    "description": "дата покупки (YYYY-MM-DD)"
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "brand", "name", "extra", "count", "price", "date"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["purchases"]
            }

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 37,
                        "message": "Карта уже привязана к другому аккаунту"
                    },
                    {
                        "code": 39,
                        "message": "К аккаунту уже привязана карта"
                    },
                    {
                        "code": 41,
                        "message": "Превышен лимит попыток верификации карты"
                    }
                ]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

### POST - получение списка последних покупок [POST]
+ Parameters
    + id: 2765000509654 (required, number) - ID карты (13 цифр)

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "first_name": "Иван",
                "last_name": "Иванов",
                "sex": 1,
                "city": "Омск",
                "dob": "1990-09-22",
                "purchases": [
                    1,
                    3
                ]
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "first_name": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "description": "имя"
                    },
                    "last_name": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "description": "фамилия"
                    },
                    "sex": {
                        "type": "number",
                        "enum": [0, 1, 2],
                        "description": "пол (0 - не выбран, 1 - женский, 2 - мужской)"
                    },
                    "city": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "description": "город"
                    },
                    "dob": {
                        "type": "string",
                        "format": "date",
                        "description": "дата рождения (YYYY-MM-DD)"
                    },
                    "purchases": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список покупок, которые выбрал пользователь",
                        "items": {
                            "type": "integer",
                            "minimum": 1,
                            "description": "идентификатор покупки"
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["first_name", "last_name", "sex", "city", "dob", "purchases"]
            }

+ Response 200 (application/json)
    + Body

            {
                "type": "club", // клубная
                "number": "2765000509654",
                "percent": 15,
                "summ": 10000,
                "increase_percent": 20,
                "increase_summ": 20000
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "type": {
                        "type": "string",
                        "enum": ["virtual", "student", "club", "vip"],
                        "description": "тип карты"
                    },
                    "number": {
                        "type": "string",
                        "minLength": 13,
                        "maxLength": 13,
                        "description": "номер карты"
                    },
                    "percent": {
                        "type": "number",
                        "minimum": 10,
                        "maximum": 25,
                        "description": "процент скидки"
                    },
                    "summ": {
                        "type": "number",
                        "minimum": 0,
                        "description": "сумма накоплений"
                    },
                    "increase_percent": {
                        "type": "number",
                        "minimum": 10,
                        "maximum": 25,
                        "description": "следующий процент скидки"
                    },
                    "increase_summ": {
                        "type": "number",
                        "minimum": 0,
                        "description": "сумма, необходимая до повышения процента скидки"
                    }                    
                },
                "additionalProperties": false,
                "required": ["type", "number", "percent", "summ"]
            } 

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 24,
                        "message": "Список покупок неверный"
                    },
                    {
                        "code": 37,
                        "message": "Карта уже привязана к другому аккаунту"
                    },
                    {
                        "code": 39,
                        "message": "К аккаунту уже привязана карта"
                    },
                    {
                        "code": 41,
                        "message": "Превышен лимит попыток верификации карты"
                    }
                ]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

# Group Addresses
## /addresses

### GET - получение списка привязанных адрессов [GET]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)
    + Body

            {
                "addresses": [
                    {
                        "id": 234,
                        // см. GET /addresses/id
                    }
                ]
            }

    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "addresses": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список адресов",
                        "items": {
                            "type": "object",
                            "properties": {
                                "id": {
                                    "type": "number",
                                    "minimum": 1,
                                    "description": "идентефикатор адреса"
                                },
                                // см. GET /addresses/id
                            },
                            "additionalProperties": false,
                            "required": ["id", 
                                // см. GET /addresses/id
                            ]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["addresses"]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

### POST - создание нового адреса [POST]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                // см. GET /addresses/id
            }

    + Schema
    
            {
                // см. GET /addresses/id
            }

+ Response 200 (application/json)
    + Body

            {
                "address_id": 12
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "address_id": {
                        "type": "number",
                        "description": "идентефикатор адреса"
                    }
                },
                "additionalProperties": false,
                "required": ["address_id"]
            }

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    }
                ]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

## /addresses/{id}

### PUT - изменение данных адресса [PUT]
+ Parameters
    + id: 2 (required, number) - ID адреса

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "region_id": 12,
                "district_id": 43,
                "city_id": 152,
                "village_id": 0,
                "street_id": 42,
                "house": 22,
                "region_text": "Омская область",
                "district_text": "Омский район",
                "city_text": "п. Новоомский",
                "street_text": "ул. Молодежная",
                "housing": "a",
                "entrance": "2",
                "room": "245",
                "zipcode": 644213,
                "first_name": "Анна",
                "last_name": "Оборина",
                "phone1": "89623042332",
                "phone2": "84953042332"
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "region_id": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 99,
                        "description": "идентефикатор области (1-й уровень КЛАДРа)"
                    },
                    "district_id": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 999,
                        "description": "идентефикатор района (2-й уровень КЛАДРа)"
                    },
                    "city_id": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 999,
                        "description": "идентефикатор города (3-й уровень КЛАДРа)"
                    },
                    "village_id": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 999,
                        "description": "идентефикатор деревни или поселка (4-й уровень КЛАДРа)"
                    },
                    "street_id": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 9999,
                        "description": "идентефикатор улицы (5-й уровень КЛАДРа)"
                    },
                    "house": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 99999,
                        "description": "номер дома (6-й уровень КЛАДРа)"
                    },
                    "region_text": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "description": "название области"
                    },
                    "district_text": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "description": "название района"
                    },
                    "city_text": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "description": "название города, деревни или поселка"
                    },
                    "street_text": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "description": "название улицы"
                    },
                    "housing": {
                        "type": "string",
                        "minLength": 0,
                        "maxLength": 8,
                        "description": "корпус"
                    },
                    "entrance": {
                        "type": "string",
                        "minLength": 0,
                        "maxLength": 8,
                        "description": "подъезд"
                    },
                    "room": {
                        "type": "string",
                        "minLength": 0,
                        "maxLength": 8,
                        "description": "квартира"
                    },
                    "zipcode": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 999999,
                        "description": "индекс"
                    },
                    "first_name": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "description": "Имя"
                    },
                    "last_name": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 64,
                        "description": "фамилия"
                    },
                    "phone1": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 32,
                        "description": "телефон"
                    },
                    "phone2": {
                        "type": "string",
                        "minLength": 0,
                        "maxLength": 32,
                        "description": "телефон (дополнительный)"
                    }
                },
                "additionalProperties": false,
                "required": ["region_id", "district_id", "city_id", "village_id", "house", 
                    "region_text", "district_text", "city_text", "street_text", "housing", "entrance", "room", 
                    "zipcode", "first_name", "last_name", "phone1", "phone2"]
            }

+ Response 200 (application/json)
    
+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    }
                ]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

+ Response 404 (application/json)

### DELETE - удаление адресса [DELETE]
+ Parameters
    + id: 2 (required, number) - ID адреса

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)
    
+ Response 401 (application/json)

+ Response 403 (application/json)

+ Response 404 (application/json)

## /addresses/regions
### POST - поиск районов по КЛАДРу [POST]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)
    + Body

            {
                "regions": [
                    {
                        "region_id": 1,
                        "region_text": "г. Москва"
                    },
                    {
                        "region_id": 2,
                        "region_text": "Московская обл."
                    }
                ]
            }

    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "regions": {
                        "type": "array",
                        "items": {
                            "type": "object",
                            "properties": {
                                "region_id": {
                                    "type": "number",
                                    "minimum": 0,
                                    "maximum": 99,
                                    "description": "идентефикатор области (1-й уровень КЛАДРа)"
                                },
                                "region_text": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 64,
                                    "description": "название области"
                                }
                            },
                            "additionalProperties": false,
                            "required": ["region_id", "region_text"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["regions"]
            }

+ Response 401 (application/json)

## /addresses/districts
### POST - поиск районов по КЛАДРу [POST]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "region_id": 77
            }

    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "region_id": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 99,
                        "description": "идентефикатор области (1-й уровень КЛАДРа)"
                    }
                },
                "additionalProperties": false,
                "required": ["region_id"]
            }

+ Response 200 (application/json)
    + Body

            {
                "districts": [
                    {
                        "district_id": 2,
                        "district_text": "р-н. Новоомский"
                    },
                    {
                        "district_id": 1,
                        "district_text": "р-н. Омский"
                    }
                ]
            }

    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "districts": {
                        "type": "array",
                        "items": {
                            "type": "object",
                            "properties": {
                                "district_id": {
                                    "type": "number",
                                    "minimum": 0,
                                    "maximum": 999,
                                    "description": "идентефикатор района (2-й уровень КЛАДРа)"
                                },
                                "district_text": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 64,
                                    "description": "название района"
                                }
                            },
                            "additionalProperties": false,
                            "required": ["district_id", "district_text"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["districts"]
            }

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 48,
                        "message": "Такой объект в КЛАДРе не найден"
                    }
                ]
            }

+ Response 401 (application/json)

## /addresses/cities
Возвращает все города, поселки и деревни для конкретного региона и района.

### POST - поиск городов, поселков и деревень по КЛАДРу [POST]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "region_id": 77,
                "district_id": 2,
            }

    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "region_id": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 99,
                        "description": "идентефикатор области (1-й уровень КЛАДРа)"
                    },
                    "district_id": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 999,
                        "description": "идентефикатор района (2-й уровень КЛАДРа)"
                    }
                },
                "additionalProperties": false,
                "required": ["region_id", "district_id"]
            }

+ Response 200 (application/json)
    + Body

            {
                "cities": [
                    {
                        "city_id": 42,
                        "village_id": 0,
                        "city_text": "г. Омск"
                    },
                    {
                        "city_id": 0,
                        "village_id": 32,
                        "city_text": "с. Седельниково"
                    }
                ]
            }

    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "cities": {
                        "type": "array",
                        "items": {
                            "type": "object",
                            "properties": {
                                "city_id": {
                                    "type": "number",
                                    "minimum": 0,
                                    "maximum": 999,
                                    "description": "идентефикатор города (3-й уровень КЛАДРа)"
                                },
                                "village_id": {
                                    "type": "number",
                                    "minimum": 0,
                                    "maximum": 99,
                                    "description": "идентефикатор деревни или поселка (4-й уровень КЛАДРа)"
                                },
                                "city_text": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 64,
                                    "description": "название города, деревни или поселка"
                                }
                            },
                            "additionalProperties": false,
                            "required": ["city_id", "village_id", "city_text"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["cities"]
            }

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 48,
                        "message": "Такой объект в КЛАДРе не найден"
                    }
                ]
            }

+ Response 401 (application/json)

## /addresses/streets
### POST - поиск улиц по КЛАДРу [POST]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "region_id": 77,
                "district_id": 2,
                "city_id": 0,
                "village_id": 42
            }

    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "region_id": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 99,
                        "description": "идентефикатор области (1-й уровень КЛАДРа)"
                    },
                    "district_id": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 999,
                        "description": "идентефикатор района (2-й уровень КЛАДРа)"
                    },
                    "city_id": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 999,
                        "description": "идентефикатор города (3-й уровень КЛАДРа)"
                    },
                    "village_id": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 999,
                        "description": "идентефикатор деревни или поселка (4-й уровень КЛАДРа)"
                    }
                },
                "additionalProperties": false,
                "required": ["region_id", "district_id", "city_id", "village_id"]
            }

+ Response 200 (application/json)
    + Body

            {
                "streets": [
                    {
                        "street_id": 42,
                        "street_text": "ул. 3-я Молодежная"
                    },
                    {
                        "street_id": 44,
                        "street_text": "бул. Просвещения"
                    }
                ]
            }

    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "streets": {
                        "type": "array",
                        "items": {
                            "type": "object",
                            "properties": {
                                "street_id": {
                                    "type": "number",
                                    "minimum": 0,
                                    "maximum": 9999,
                                    "description": "идентефикатор улицы (5-й уровень КЛАДРа)"
                                },
                                "street_text": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 64,
                                    "description": "название улицы"
                                }
                            },
                            "additionalProperties": false,
                            "required": ["street_id", "street_text"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["zipcode"]
            }

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 48,
                        "message": "Такой объект в КЛАДРе не найден"
                    }
                ]
            }

+ Response 401 (application/json)

## /addresses/zipcode
### POST - поиск индекса по КЛАДРу [POST]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "region_id": 77,
                "district_id": 2,
                "city_id": 0,
                "village_id": 42,
                "street_id": 72,
                "house": 2
            }

    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "region_id": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 99,
                        "description": "идентефикатор области (1-й уровень КЛАДРа)"
                    },
                    "district_id": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 999,
                        "description": "идентефикатор района (2-й уровень КЛАДРа)"
                    },
                    "city_id": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 999,
                        "description": "идентефикатор города (3-й уровень КЛАДРа)"
                    },
                    "village_id": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 999,
                        "description": "идентефикатор деревни или поселка (4-й уровень КЛАДРа)"
                    },
                    "street_id": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 9999,
                        "description": "идентефикатор улицы (5-й уровень КЛАДРа)"
                    },
                    "house": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 99999,
                        "description": "номер дома (6-й уровень КЛАДРа)"
                    }
                },
                "additionalProperties": false,
                "required": ["region_id", "district_id", "city_id", "village_id", "street_id", "house"]
            }

+ Response 200 (application/json)
    + Body

            {
                "zipcode": 644213
            }

    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "zipcode": {
                        "type": "number",
                        "minLength": 0,
                        "maxLength": 999999,
                        "description": "индекс"
                    }
                },
                "additionalProperties": false,
                "required": ["zipcode"]
            }

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 48,
                        "message": "Такой объект в КЛАДРе не найден"
                    }
                ]
            }

+ Response 401 (application/json)

## /addresses/search
Все гео-объекты разделены на уровни: регион, область, город, деревня, улица, дом, индекс.
Уровень объектов в возвращаемом списке зависит от входящих параметров. Если параметры не
передавать, возвратиться список регионов (объекты самого верхнего уровня). Если передать ID
региона, можно получить список областей. Если передать ID региона + ID области, вернётся
список городов и т.д. Если передать полный набор допустимых параметров, включая дом, вернётся
почтовый индекс.

В связи с тем, что некоторые города (например, Москва) имеют уровень региона, запрос c {"region_id":"77"},
вернёт пустой список областей. Для получения списка улиц Москвы нужно передать
{"region_id":"77","district_id":"000","city_id":"000"}. Либо сокращённую форму
{"region_id":"77","city_id":"000"}. Пропущенный параметр уровня области (district_id) на
стороне сервера будет автоматически добавлен со значением "000".

Примеры:

{} - список регионов

{"region_id":"55"} - районы Омской обл.

{"region_id":"55","district_id":"032"} - города Черлакского района Омской обл.

{"region_id":"55","district_id":"032","village_id":"001"} - улицы рп.Черлак

{"region_id":"55","district_id":"032","village_id":"001","street_id":"0004","house":"1"} - почтовый индекс дома №1 по ул.Крестьянская 1-я в рп.Черлак

### {? удалить} POST - Поиск гео-объектов по КЛАДРy [POST]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "region_id": "77",
                "district_id": "000",
                "city_id": "000",
                "village_id": "000",
                "street_id": "0072",
                "house": "2"
            }

    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "region_id": {
                        "type": "string",
                        "minLength": 2,
                        "maxLength": 2,
                        "pattern": "^\\d{2}$"
                    },
                    "district_id": {
                        "type": "string",
                        "minLength": 3,
                        "maxLength": 3,
                        "pattern": "^\\d{3}$"
                    },
                    "city_id": {
                        "type": "string",
                        "minLength": 3,
                        "maxLength": 3,
                        "pattern": "^\\d{3}$"
                    },
                    "village_id": {
                        "type": "string",
                        "minLength": 3,
                        "maxLength": 3,
                        "pattern": "^\\d{3}$"
                    },
                    "street_id": {
                        "type": "string",
                        "minLength": 4,
                        "maxLength": 4,
                        "pattern": "^\\d{4}$"
                    },
                    "house": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 5,
                        "pattern": "^\\d{1,5}$"
                    }
                },
                "additionalProperties": false
            }

+ Response 200 (application/json)
    + Body

            {
                // ???
            }

    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "array",
                "minItems": 1,
                "items": {
                    "type": "object",
                    "properties": {
                        "region_id": {
                            "type": "string",
                            "minLength": 2,
                            "maxLength": 2,
                            "pattern": "^\\d{2}$"
                        },
                        "district_id": {
                            "type": "string",
                            "minLength": 3,
                            "maxLength": 3,
                            "pattern": "^\\d{3}$"
                        },
                        "city_id": {
                            "type": "string",
                            "minLength": 3,
                            "maxLength": 3,
                            "pattern": "^\\d{3}$"
                        },
                        "village_id": {
                            "type": "string",
                            "minLength": 3,
                            "maxLength": 3,
                            "pattern": "^\\d{3}$"
                        },
                        "street_id": {
                            "type": "string",
                            "minLength": 4,
                            "maxLength": 4,
                            "pattern": "^\\d{4}$"
                        },
                        "region_prefix": {
                            "type": "string",
                            "minLength": 1,
                            "maxLength": 32
                        },
                        "region_text": {
                            "type": "string",
                            "minLength": 1,
                            "maxLength": 255
                        },
                        "district_prefix": {
                            "type": "string",
                            "minLength": 1,
                            "maxLength": 32
                        },
                        "district_text": {
                            "type": "string",
                            "minLength": 1,
                            "maxLength": 255
                        },
                        "city_prefix": {
                            "type": "string",
                            "minLength": 1,
                            "maxLength": 32
                        },
                        "city_text": {
                            "type": "string",
                            "minLength": 1,
                            "maxLength": 255
                        },
                        "village_prefix": {
                            "type": "string",
                            "minLength": 1,
                            "maxLength": 32
                        },
                        "village_text": {
                            "type": "string",
                            "minLength": 1,
                            "maxLength": 255
                        },
                        "index": {
                            "type": "string",
                            "minLength": 6,
                            "maxLength": 6,
                            "pattern": "^\\d{6}$"
                        }
                    }
                },
                "additionalProperties": false
            }

+ Response 401 (application/json)

## /addresses/delivery

### POST - Калькульятор доставки [POST]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "region_id": 77,
                "district_id": 0,
                "city_id": 0,
                "village_id": 0
            }

    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                     "region_id": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 99,
                        "description": "идентефикатор области (1-й уровень КЛАДРа)"
                    },
                    "district_id": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 999,
                        "description": "идентефикатор района (2-й уровень КЛАДРа)"
                    },
                    "city_id": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 999,
                        "description": "идентефикатор города (3-й уровень КЛАДРа)"
                    },
                    "village_id": {
                        "type": "number",
                        "minimum": 0,
                        "maximum": 999,
                        "description": "идентефикатор деревни или поселка (4-й уровень КЛАДРа)"
                    }
                },
                "additionalProperties": false,
                "required": ["region_id", "district_id", "city_id", "village_id"]
            }

+ Response 200 (application/json)
    + Body

            {
                "methods": [
                    {
                        "id": 1,
                        "text": "Курьерская доставка",
                        "price": 0,
                        "min_days": 1,
                        "max_days": 2
                    },
                    {
                        "id": 2,
                        "text": "Экспресс-доставка",
                        "price": 500,
                        "min_days": 1,
                        "max_days": 1
                    }
                ]
            }

    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "methods": {
                        "type": "array",
                        "minItems": 1,
                        "items": {
                            "type": "object",
                            "properties": {
                                "id": {
                                    "type": "number",
                                    "minimum": 0,
                                    "maximum": 2147483647,
                                    "description": "идентификатор способа доставки"
                                },
                                "text": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 255,
                                    "description": "название способа доставки"
                                },
                                "price": {
                                    "type": "number",
                                    "minimum": 0,
                                    "maximum": 2147483647,
                                    "description": "стоимость доставки в рублях"
                                },
                                "min_days": {
                                    "type": "number",
                                    "minimum": 1,
                                    "maximum": 2147483647,
                                    "description": "минимальное количество дней"
                                },
                                "max_days": {
                                    "type": "number",
                                    "minimum": 1,
                                    "maximum": 2147483647,
                                    "description": "максимальное количество дней"
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "text", "price", "min_days", "max_days"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["methods"]
            }

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 48,
                        "message": "Такой объект в КЛАДРе не найден"
                    }
                ]
            }

+ Response 401 (application/json)

# Group Cart

## /cart

### GET - получение данных для корзины [GET]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)
    + Body

            {
                "summ": "30250.00",
                "discount": "150.00",
                "promocode": "IQDSFE34RE3", // промо-код на скидку, необязательное поле
                "promocode_discount": "25", // процент скидки по промо-коду для надписи "Промо-код со скидкой 25% активирован", необязательное поле
                "products": [
                    {
                        "id": 100,
                        "product_id": 10000,
                        "group_id": 12345,
                        "count": 1,
                        "brand": "SHISEDO",
                        "name": "Benifiance",
                        "articul": "L9171300",
                        "extra": "Poppin' Poppy", 
                        "volume": "5мл", 
                        "pictures": [
                            {
                                "width": 500,
                                "height": 500,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_4/97608/pre/112_68sb.jpg"
                            }
                        ],
                        "price": {
                            "type": 1,
                            "min": 75,
                            "max": 100,
                            "not_available": "temp"
                        }
                    }
                ],
                "gifts": [
                    {
                        "id": 5235,
                        "brand": "LANCOME",
                        "name": "Миниатюра Advanced Genigique Light-Pearl",
                        "articul": "L5814600LRL",
                        "pictures": [
                            {
                                "width": 500,
                                "height": 500,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_4/97608/pre/112_68sb.jpg"
                            }
                        ],
                        "promocode": "IQDSFE34RE3" // необязательное поле
                    }
                ]
            }

    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "summ": {
                        "type": "string",
                        "description": "сумма цен товаров в корзине (в формате 3123.10)"
                    },
                    "discount": {
                        "type": "string",
                        "description": "скидка (в формате 3123.10)"
                    },
                    "promocode": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 16,
                        "description": "промо-код на скидку"
                    },
                    "promocode_discount": {
                        "type": "string",
                        "description": "процент скидки по промо-коду для надписи --Промо-код со скидкой 25% активирован--"
                    },
                    "products": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "товары в корзине",
                        "items": {
                            "type": "object",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор товара в корзине"
                                },
                                "product_id": {
                                    "type": "integer",
                                    "description": "идентификатор товара в каталоге"
                                },
                                "group_id": {
                                    "type": "integer",
                                    "description": "идентификатор товарной группы"
                                },
                                "count": {
                                    "type": "number",
                                    "minimum": 1,
                                    "maximum": 5,
                                    "description": "количество экземпляров товара"
                                },
                                "brand": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название бренда товара"
                                },
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название товара"
                                },
                                "articul": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 16,
                                    "description": "артикул"
                                },
                                "extra": {
                                    "type": "string",
                                    "minLength": 0,
                                    "maxLength": 128,
                                    "description": "строка с доп. текстом: название оттенка, аксессуара и прочее"
                                },
                                "volume": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 16,
                                    "description": "объем"
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "price": {
                                    "type": "object",
                                    "description": "описание цены",
                                    "properties": {
                                        "type": {
                                            "type": "integer",
                                            "enum": [1, 2, 3],
                                            "description": "тип цены на товар: 1 - диапазон цен, возможна скидка по карте, 2 - фиксированная скидка, 3 - фиксированная цена"
                                        },
                                        "min": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "минимальная цена"
                                        },
                                        "max": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "максимальная цена"
                                        },
                                        "not_available": {
                                            "type": "string",
                                            "enum": ["temp", "perm"],
                                            "description": "'временно нет в наличии' или 'нет в продаже', если параметр не определен, то товар доступен для покупки"
                                        }
                                    },
                                    "additionalProperties": false,
                                    "required": ["type", "min", "max"]
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "product_id", "group_id", "count", "brand", 
                                         "name", "articul", "extra", "pictures", "price"]
                        }
                    },
                    "gifts": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "подарки",
                        "items": {
                            "type": "object",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор подарка"
                                },
                                "brand": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название бренда подарка"
                                },
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название подарка"
                                },
                                "articul": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 16,
                                    "description": "артикул"
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "promocode": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 16,
                                    "description": "промо-код, по которому получен подарок"
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "brand", "name", "articul", "pictures"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["summ", "discount", "products", "gifts"]
            }

+ Response 401 (application/json)

### POST - добавление товара в корзину [POST]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "product_id": 10000
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "product_id": {
                        "type": "number",
                        "minimum": 1,
                        "maximum": 2147483647,                        
                        "description": "идентификатор товара"
                    }
                },
                "additionalProperties": false,
                "required": ["product_id"]

            }

+ Response 200 (application/json)
    + Body

            {
                // возвращается обновленное описание корзины, см GET /cart
            }

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 26,
                        "message": "Превышен лимит суммы заказа"
                    },
                    {
                        "code": 27,
                        "message": "Превышен лимит количества одинаковых товаров в заказе"
                    },
                    {
                        "code": 28,
                        "message": "Товара нет в наличии"
                    },
                    {
                        "code": 29,
                        "message": "Товара не существует"
                    }
                ]
            }

+ Response 401 (application/json)

## /cart/{id}

### PUT - изменение количества товара [PUT]
+ Parameters
    + id: 1963896 (required, number) - ID продукта

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "count": 2
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "count": {
                        "type": "number",
                        "minimum": 1,
                        "maximum": 5,
                        "description": "количество экземпляров товара"
                    }
                },
                "additionalProperties": false,
                "required": ["count"]
            }

+ Response 200 (application/json)
    + Body

            {
                // возвращается обновленное описание корзины, см. GET /cart
            }

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 26,
                        "message": "Превышен лимит суммы заказа"
                    },
                    {
                        "code": 27,
                        "message": "Превышен лимит количества одинаковых товаров в заказе"
                    },
                    {
                        "code": 28,
                        "message": "Товара нет в наличии"
                    }
                ]
            }

+ Response 401 (application/json)

+ Response 404 (application/json)

### DELETE - удаление товара из корзины [DELETE]
+ Parameters
    + id: 1963896 (required, number) - ID продукта

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)
    + Body

            {
                // возвращается обновленное описание корзины, см GET /cart
            }
            
+ Response 401 (application/json)

+ Response 404 (application/json)

## /cart/promo

### POST - отправка промо-кода [POST]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "promocode": "IQDSFE34RE3"
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "promocode": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 16,
                        "description": "промо-код"
                    }
                },
                "additionalProperties": false,
                "required": ["promocode"]
            }

+ Response 200 (application/json)
    + Body

            {
                // возвращается обновленное описание корзины, см GET /cart
            }

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 30,
                        "message": "Промо-код уже активирован другим пользователем"
                    },
                    {
                        "code": 31,
                        "message": "У промо-кода истек срок активации"
                    },
                    {
                        "code": 34,
                        "message": "Промо-кода не существует"
                    },
                    {
                        "code": 40,
                        "message": "Для корзины в данный момент уже применен другой промо-код"
                    }
                ]
            }

+ Response 401 (application/json)

### DELETE - удаление промо-кода [DELETE]
Если в корзине не был использован промо-код, возвращается 404 ошибка.
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)
    + Body

            {
                // возвращается обновленное описание корзины, см GET /cart
            }

+ Response 401 (application/json)

+ Response 404 (application/json)

# Group Wishlist

## /wishlist

### GET - получение списка товаров в wishlist [GET]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)
    + Body

            {
                "products": [
                    {
                        "id": 100,
                        "product_id": 10000,
                        "group_id": 12345,
                        "brand": "SHISEDO",
                        "name": "Benifiance",
                        "articul": "L9171300",
                        "extra": "Poppin' Poppy",
                        "volume": "50мл",
                        "pictures": [
                            {
                                "width": 500,
                                "height": 500,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_4/97608/pre/112_68sb.jpg"
                            }
                        ],
                        "price": {
                            "type": 1,
                            "min": 75,
                            "max": 100,
                            "not_available": "temp"
                        }
                    }
                ],
                "sharing_url": "http://...."
            }

    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "products": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "товары в wishlist",
                        "items": {
                            "type": "object",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор товара в корзине"
                                },
                                "product_id": {
                                    "type": "integer",
                                    "description": "идентификатор товара в каталоге"
                                },
                                "group_id": {
                                    "type": "integer",
                                    "description": "идентификатор группы товаров"
                                },
                                "brand": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название бренда товара"
                                },
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название товара"
                                },
                                "articul": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 16,
                                    "description": "артикул"
                                },
                                "extra": {
                                    "type": "string",
                                    "minLength": 0,
                                    "maxLength": 128,
                                    "description": "строка с доп. текстом: название оттенка, аксессуара и прочее"
                                },
                                "volume": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 16,
                                    "description": "объем"
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "price": {
                                    "type": "object",
                                    "description": "описание цены",
                                    "properties": {
                                        "type": {
                                            "type": "integer",
                                            "enum": [1, 2, 3],
                                            "description": "тип цены на товар: 1 - диапазон цен, возможна скидка по карте, 2 - фиксированная скидка, 3 - фиксированная цена"
                                        },
                                        "min": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "минимальная цена"
                                        },
                                        "max": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "максимальная цена"
                                        },
                                        "not_available": {
                                            "type": "string",
                                            "enum": ["temp", "perm"],
                                            "description": "'временно нет в наличии' или 'нет в продаже', если параметр не определен, то товар доступен для покупки"
                                        }
                                    },
                                    "additionalProperties": false,
                                    "required": ["type", "min", "max"]
                                }
                            }
                        },
                        "additionalProperties": false,
                        "required": ["id", "product_id", "group_id", "brand", "name", "articul", "extra", "pictures", "price"]
                    },
                    "sharing_url": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 1024,
                        "format": "uri",
                        "description": "ссылка на страницу вишлиста для шаринга"
                    }
                },
                "additionalProperties": false,
                "required": ["products"]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

### POST - добавление товара в wishlist [POST]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "product_id": 10000
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "product_id": {
                        "type": "number",
                        "description": "идентификатор продукта"
                    }
                },
                "additionalProperties": false,
                "required": ["product_id"]
            }

+ Response 200 (application/json)

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 33,
                        "message": "Товар уже есть в Wishlist"
                    }
                ]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

## /wishlist/{id}

### DELETE - удаление продукта из wishlist [DELETE]
+ Parameters
    + id: 252640 (required, number) - ID продукта

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)

+ Response 401 (application/json)

+ Response 403 (application/json)

+ Response 404 (application/json)

# Group Orders

## /orders

### GET - получение списка заказов [GET]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)

    + Body

            {
                "orders": [
                    {
                        "order_id": 213,
                        "count": 4,
                        "summ": "30015.00",                        
                        "maked_date": "2016-01-19",
                        "status": 1, // 1 - на сборке, 2 - в пути, 3 - доставлен
                        "status_date": "2016-02-01"
                    }
                ]
            }

    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "orders": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список заказов",
                        "items": {
                            "type": "object",
                            "properties": {
                                "order_id": {
                                    "type": "integer",
                                    "description": "номер заказа"
                                },
                                "count": {
                                    "type": "integer",
                                    "description": "количество товаров в заказе"
                                },
                                "summ": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 16,
                                    "description": "сумма заказа (12124.00)"
                                },
                                "maked_date": {
                                    "type": "string",
                                    "format": "date",
                                    "description": "дата оформления заказа (YYYY-MM-DD)"
                                },
                                "status": {
                                    "type": "string",
                                    "enum": [1, 2, 3],
                                    "description": "статус заказа (1 - на сборке, 2 - в пути, 3 - доставлен)"
                                },
                                "status_date": {
                                    "type": "string",
                                    "format": "date",
                                    "description": "дата последнего изменения статуса (YYYY-MM-DD)"
                                }
                            },
                            "additionalProperties": false,
                            "required": ["order_id", "count", "summ", "maked_date", "status", "status_date"]
                        }
                    } 
                },
                "additionalProperties": false,
                "required": ["orders"]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

### {?} POST - отправка данных о заказе [POST]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "address_id": 100,
                "delivery_method": 1,
                "pay_method": 1
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "address_id": {
                        "type": "number",
                        "minimum": 1,
                        "description": "идентификатор адреса"
                    },
                    "delivery_method": {
                        "type": "number",
                        "minimum": 1,
                        "description": "идентификатор способа доставки"
                    },
                    "pay_method": {
                        "type": "number",
                        "minimum": 1,
                        "description": "идентификатор способа оплаты"
                    }
                },
                "additionalProperties": false,
                "required": ["address_id", "delivery_method", "pay_method"]
            }

+ Response 200 (application/json)

    + Body

            {
                "order_id": 213,
                "pay_link": "https://ya.ru"   // необязательный параметр, возвращается, если выбрана оплата электронным способом
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "order_id": {
                        "type": "number",
                        "description": "идентификатор заказа"
                    },
                    "pay_link": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 1024,
                        "format": "uri",
                        "description": "ссылка на страницу оплаты"
                    }
                },
                "additionalProperties": false,
                "required": ["order_id"]
            }
            
+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    }
                ]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

## /orders/{id}

### GET - получение данных о заказе [GET]
+ Parameters
    + id: 4124 (required, number) - ID заказа

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)

    + Body

            {
                "order_id": 213,
                "summ": 30015,                        
                "maked_date": "2016-01-19",
                "status": 1, // 1 - на сборке, 2 - в пути, 3 - доставлен
                "status_date": "2016-02-01",
                "address": {
                    // см. POST /addresses
                },
                "delivery_method": 1,
                "pay_method": 1,
                "products": [
                    {
                        "product_id": 10000,
                        "group_id": 2414,
                        "count": 1,
                        "brand": "SHISEDO",
                        "name": "Benifiance",
                        "articul": "L9171300",
                        "extra": "Poppin' Poppy",
                        "volume": "50мл",
                        "pictures": [
                            {
                                "width": 500,
                                "height": 500,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_4/97608/pre/112_68sb.jpg"
                            }
                        ],
                        "price": "3442.40"
                    }
                ]
            }

    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "order_id": {
                        "type": "integer",
                        "description": "номер заказа"
                    },
                    "count": {
                        "type": "integer",
                        "description": "количество товаров в заказе"
                    },
                    "summ": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 16,
                        "description": "сумма заказа (12124.00)"
                    },
                    "maked_date": {
                        "type": "string",
                        "format": "date",
                        "description": "дата оформления заказа (YYYY-MM-DD)"
                    },
                    "status": {
                        "type": "string",
                        "enum": [1, 2, 3],
                        "description": "статус заказа (1 - на сборке, 2 - в пути, 3 - доставлен)"
                    },
                    "status_date": {
                        "type": "string",
                        "format": "date",
                        "description": "дата последнего изменения статуса (YYYY-MM-DD)"
                    },
                    "address": {
                        // см. POST /addresses
                    },
                    "delivery_method": {
                        "type": "number",
                        "minimum": 1,
                        "description": "идентификатор способа доставки"
                    },
                    "pay_method": {
                        "type": "number",
                        "minimum": 1,
                        "description": "идентификатор способа оплаты"
                    },
                    "products": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "продукты в заказе",
                        "items": {
                            "type": "object",
                            "properties": {
                                "product_id": {
                                    "type": "integer",
                                    "description": "идентификатор товара в каталоге"
                                },
                                "group_id": {
                                    "type": "integer",
                                    "description": "идентификатор группы товаров"
                                },
                                "count": {
                                    "type": "number",
                                    "minimum": 1,
                                    "maximum": 5,
                                    "description": "количество экземпляров товара"
                                },
                                "brand": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название бренда товара"
                                },
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название товара"
                                },
                                "articul": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 16,
                                    "description": "артикул"
                                },
                                "extra": {
                                    "type": "string",
                                    "minLength": 0,
                                    "maxLength": 128,
                                    "description": "строка с доп. текстом: название оттенка, аксессуара и прочее"
                                },
                                "volume": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 16,
                                    "description": "объем"
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "price": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 16,
                                    "description": "цена продукта при покупке (234234.30)"
                                }
                            }
                        },
                        "additionalProperties": false,
                        "required": ["product_id", "group_id", "count", "brand", "name", "articul", "extra", "pictures", "price"]
                    } 

                },
                "additionalProperties": false,
                "required": ["order_id", "count", "summ", "maked_date", "status", "status_date", 
                            "address", "delivery_method", "pay_method", "products"]
            }

+ Response 401 (application/json)

+ Response 403 (application/json)

+ Response 404 (application/json)

# Group Chanel

## /chanel/catalog

### GET - получение каталога Chanel [GET]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)

    + Body

            {
                "catalog": [
                    {
                        "id": 1,
                        "name": "ПАРФЮМЕРИЯ",
                        "subs": [
                            {
                                "id": 101,
                                "name": "Женская парфюмерия",
                                "subs": [
                                    {
                                        "id": 20101,
                                        "name": "№5"
                                    },
                                    {
                                        "id": 20102,
                                        "name": "COCO MADEMOISELLE"
                                    },
                                    {
                                        "id": 20103,
                                        "name": "CHANCE"
                                    },
                                    {
                                        "id": 20104,
                                        "name": "CHANCE EAU FRAÎCHE"
                                    },
                                    {
                                        "id": 20105,
                                        "name": "CHANCE EAU TENDRE"
                                    },
                                    {
                                        "id": 20106,
                                        "name": "CHANCE EAU VIVE"
                                    },
                                    {
                                        "id": 20107,
                                        "name": "COCO NOIR"
                                    },
                                    {
                                        "id": 20108,
                                        "name": "COCO"
                                    },
                                    {
                                        "id": 20108,
                                        "name": "ALLURE"
                                    }
                                ]
                            },
                            {
                                "id": 102,
                                "name": "Мужская парфюмерия",
                                "subs": []
                            }
                        ]
                    },
                    {
                        "id": 2,
                        "name": "МАКИЯЖ",
                        "subs": [
                            {
                                "id": 201,
                                "name": "Лицо",
                                "subs": [
                                    {
                                        "id": 20101,
                                        "name": "Основа под макияж"
                                    },
                                    {
                                        "id": 20102,
                                        "name": "Солнечный макияж"
                                    },
                                    {
                                        "id": 20103,
                                        "name": "Корректор"
                                    },
                                    {
                                        "id": 20104,
                                        "name": "Тональные средства"
                                    },
                                    {
                                        "id": 20105,
                                        "name": "Пудры"
                                    },
                                    {
                                        "id": 20106,
                                        "name": "Румяна"
                                    },
                                    {
                                        "id": 20107,
                                        "name": "Макияж с эффектом естественного сияния"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
    
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "catalog": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список категорий каталога 1 уровня",
                        "items": {
                            "type": "object",
                            "description": "категория каталога 1 уровня",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор категории"
                                },
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название категории"
                                },
                                "subs": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список подкатегорий второго уровня",
                                    "items": {
                                        "type": "object",
                                        "description": "подкатегория второго уровня",
                                        "properties": {
                                           "id": {
                                                "type": "integer",
                                                "description": "идентификатор подкатегории"
                                            },
                                            "name": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 128,
                                                "description": "название подкатегории"
                                            },
                                            "subs": {
                                                "type": "array",
                                                "uniqueItems": true,
                                                "description": "список подкатегорий третьего уровня",
                                                "items": {
                                                    "type": "object",
                                                    "description": "подкатегория третьего уровня",
                                                    "properties": {
                                                        "id": {
                                                            "type": "integer",
                                                            "description": "идентификатор подкатегории"
                                                        },
                                                        "name": {
                                                            "type": "string",
                                                            "minLength": 1,
                                                            "maxLength": 128,
                                                            "description": "название подкатегории"
                                                        }
                                                    },
                                                    "additionalProperties": false,
                                                    "required": ["id", "name"]
                                                }
                                            } 
                                        },
                                        "additionalProperties": false,
                                        "required": ["id", "name"]
                                    }
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "name", "subs"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["catalog"]
            }


+ Response 401 (application/json)

## /chanel/catalog/banners
{?} Пока используются только первый баннер в массиве только для категорий первого уровня.
Возможно расширение до листалки баннеров на каждом из мест. Но только для категорий первого уровня.

### GET - получение баннеров для показа внутри каталога Chanel [GET]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)

    + Body

            {
                "banners": [
                    {
                        "category_id": 1,
                        "id": 103,
                        "pictures": [
                            {
                                "width": 500,
                                "height": 500,
                                "url": "http://iledebeaute.ru/@/project/fit/539x322_sale_jan16.jpg"
                            }
                        ],
                        "action": {
                            // см. описание Actions 
                        }
                    },
                    {
                        "category_id": 101,
                        "id": 105,
                        "pictures": [
                            {
                                "width": 500,
                                "height": 500,
                                "url": "http://iledebeaute.ru/@/project/fit/tizer-im-abeille-190115.jpg"
                            }
                        ],
                        "action": {
                            // см. описание Actions
                        }
                    }
                ]
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "banners": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список баннеров",
                        "items": {
                            "type": "object",
                            "description": "баннер",
                            "properties": {
                                "category_id": {
                                    "type": "integer",
                                    "description": "идентификатор категории каталога"
                                },
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор баннера"
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "action": {
                                    // см. описание Actions
                                }
                            },
                            "additionalProperties": false,
                            "required": ["category_id", "id", "pictures", "action"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["banners"]
            }       

+ Response 401 (application/json)

## /chanel/general
Метод получает баннер для главной страницы бренда и список товаров: Новинки и Бестселлеры. 
{?} Пока используется только первый из списка баннеров.

### GET - получение данных для главного экрана Chanel [GET]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)

    + Body

            {
                "banners": [
                    {
                        "id": 103,
                        "pictures": [
                            {
                                "width": 500,
                                "height": 500,
                                "url": "http://iledebeaute.ru/@/project/fit/539x322_sale_jan16.jpg"
                            }
                        ],
                        "action": {
                            // см. описание Actions 
                        }
                    }
                ],
                "groups_new": [
                    {
                        "id": 1,
                        "brand": "CHANEL",
                        "name": "CHANEL ROUGE COCO SHINE SPRING 2016",
                        "text": "Увлажняющая губная помада с блеском",
                        "pictures": [
                            {
                                "width": 156,
                                "height": 156,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_5/115760/pre/156_156sb.jpg"
                            },
                            {
                                "width": 257,
                                "height": 257,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_5/115760/pre/257_257sb.jpg"
                            }
                        ],
                        "price": {
                            "type": 1,
                            "min": 75,
                            "max": 100,
                            "not_available": "temp"
                        }
                    }
                ],
                "groups_best": [
                    {
                        "id": 1,
                        "brand": "CHANEL",
                        "name": "CHANEL ROUGE COCO SHINE SPRING 2016",
                        "text": "Увлажняющая губная помада с блеском",
                        "pictures": [
                            {
                                "width": 156,
                                "height": 156,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_5/115760/pre/156_156sb.jpg"
                            },
                            {
                                "width": 257,
                                "height": 257,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_5/115760/pre/257_257sb.jpg"
                            }
                        ],
                        "price": {
                            "type": 1,
                            "min": 75,
                            "max": 100,
                            "not_available": "temp"
                        }
                    }
                ]
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "banners": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список баннеров",
                        "items": {
                            "type": "object",
                            "description": "баннер",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор баннера"
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "action": {
                                    // см. описание Actions
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "pictures", "action"]
                        }
                    },
                    "groups_new": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список товарных групп в блоке Новинки",
                        "items": {
                            "type": "object",
                            "description": "группа",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор товарной группы"
                                },
                                "brand": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название бренда"
                                },
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название товарной группы"
                                },
                                "text": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "описание товарной группы"
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "price": {
                                    "type": "object",
                                    "description": "описание цены",
                                    "properties": {
                                        "type": {
                                            "type": "integer",
                                            "enum": [1, 2, 3],
                                            "description": "тип цены на товар: 1 - диапазон цен, возможна скидка по карте, 2 - фиксированная скидка, 3 - фиксированная цена"
                                        },
                                        "min": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "минимальная цена"
                                        },
                                        "max": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "максимальная цена"
                                        },
                                        "not_available": {
                                            "type": "string",
                                            "enum": ["temp", "perm"],
                                            "description": "'временно нет в наличии' или 'нет в продаже', если параметр не определен, то товар доступен для покупки"
                                        }
                                    },
                                    "additionalProperties": false,
                                    "required": ["type", "min", "max"]
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "brand", "name", "text", "pictures", "price"]
                        }
                    },
                    "groups_best": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список товарных групп в блоке Бестселлеры",
                        "items": {
                            "type": "object",
                            "description": "группа",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор товарной группы"
                                },
                                "brand": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название бренда"
                                },
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название товарной группы"
                                },
                                "text": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "описание товарной группы"
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "price": {
                                    "type": "object",
                                    "description": "описание цены",
                                    "properties": {
                                        "type": {
                                            "type": "integer",
                                            "enum": [1, 2, 3],
                                            "description": "тип цены на товар: 1 - диапазон цен, возможна скидка по карте, 2 - фиксированная скидка, 3 - фиксированная цена"
                                        },
                                        "min": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "минимальная цена"
                                        },
                                        "max": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "максимальная цена"
                                        },
                                        "not_available": {
                                            "type": "string",
                                            "enum": ["temp", "perm"],
                                            "description": "'временно нет в наличии' или 'нет в продаже', если параметр не определен, то товар доступен для покупки"
                                        }
                                    },
                                    "additionalProperties": false,
                                    "required": ["type", "min", "max"]
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "brand", "name", "text", "pictures", "price"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["banners", "groups_new", "groups_best"]
            }       

+ Response 401 (application/json)

## /chanel/catalog/groups
Группы товаров в категории каталога Chanel.
Возвращает описания групп товаров страницами по 36 групп товаров на странице.
Коллекции должны работать аналогично категориям товаров.
{?} - на текущем этапе реализации приложения в списке **categories** можно передавать только один элемент: 
остальные не будут использоваться сервером при поиске.

### POST - получение списка групп товаров [POST]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "categories": [
                    101
                ],
                "sort": "name",
                "page": 1 // необязательный параметр
            }
            
    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "categories": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список категорий",
                        "items": {
                            "type": "integer",
                            "description": "идентификатор категории",
                        }
                    },
                    "sort": {
                        "type": "string",
                        "enum": ["name", "cost_desc", "cost_asc", "brand"],
                        "description": "тип сортировки"
                    },
                    "page": {
                        "type": "integer",
                        "minimum": 1,
                        "description": "номер страницы результатов (по 36 товарных групп на странице)"
                    }
                },
                "additionalProperties": false,
                "required": ["categories", "sort"]
            }

+ Response 200 (application/json)
    + Body

            {
                "groups_count": 14,
                "sort": "name",
                "page": 1,
                "groups": [
                    {
                        "id": 1,
                        "brand": "CHANEL",
                        "name": "CHANEL ROUGE COCO SHINE SPRING 2016",
                        "text": "Увлажняющая губная помада с блеском",
                        "pictures": [
                            {
                                "width": 156,
                                "height": 156,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_5/115760/pre/156_156sb.jpg"
                            },
                            {
                                "width": 257,
                                "height": 257,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_5/115760/pre/257_257sb.jpg"
                            }
                        ],
                        "price": {
                            "type": 1,
                            "min": 75,
                            "max": 100,
                            "not_available": "temp"
                        }
                    }
                ]
            }
    
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "groups_count": {
                        "type": "integer",
                        "minimum": 1,
                        "description": "общее количество товарных групп в результатах поиска"
                    },
                    "sort": {
                        "type": "string",
                        "enum": ["name", "cost_desc", "cost_asc", "brand"],
                        "description": "тип сортировки"
                    },
                    "page": {
                        "type": "integer",
                        "minimum": 1,
                        "description": "номер страницы результатов (по 36 товарных групп на странице)"
                    },
                    "groups": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список товарных групп в данной категории",
                        "items": {
                            "type": "object",
                            "description": "группа",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор товарной группы"
                                },
                                "brand": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название бренда"
                                },
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название товарной группы"
                                },
                                "text": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "описание товарной группы"
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "price": {
                                    "type": "object",
                                    "description": "описание цены",
                                    "properties": {
                                        "type": {
                                            "type": "integer",
                                            "enum": [1, 2, 3],
                                            "description": "тип цены на товар: 1 - диапазон цен, возможна скидка по карте, 2 - фиксированная скидка, 3 - фиксированная цена"
                                        },
                                        "min": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "минимальная цена"
                                        },
                                        "max": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "максимальная цена"
                                        },
                                        "not_available": {
                                            "type": "string",
                                            "enum": ["temp", "perm"],
                                            "description": "'временно нет в наличии' или 'нет в продаже', если параметр не определен, то товар доступен для покупки"
                                        }
                                    },
                                    "additionalProperties": false,
                                    "required": ["type", "min", "max"]
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "brand", "name", "text", "pictures", "price"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["groups_count", "sort", "page", "groups"]
            }

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 23,
                        "message": "Не определен один из фильров или одна из категорий"
                    },
                    {
                        "code": 35,
                        "message": "Запрошенной страницы разультатов не существует"
                    }
                ]
            }

+ Response 401 (application/json)

## /chanel/catalog/groups/{id}

### GET - получение списка товаров в группе [GET]
+ Parameters
    + id: 1231 (required, number) - ID группы товаров

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==
    
    
+ Response 200 (application/json)
    + Body

            {                
                "id": 88953,
                "brand": "CHANEL",
                "name": "LE TOP COAT",
                "text": "Защитное покрытие для ногтей, придающее блеск",
                "pictures": [
                    {
                        "width": 156,
                        "height": 156,
                        "url": "http://static.iledebeaute.ru/files/images/tag/part_5/115760/pre/156_156sb.jpg"
                    },
                    {
                        "width": 500,
                        "height": 500,
                        "url": "http://static.iledebeaute.ru/files/images/tag/part_5/115760/pre/257_257sb.jpg"
                    }
                ],
                "products": [
                    {
                        "id": 10000,
                        "extra": "LE TOP COAT Защитное покрытие для ногтей, придающее блеск ",
                        "volume": "13 мл",
                        "photos": [
                            {
                                "width": 500,
                                "height": 500,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_4/97608/pre/112_68sb.jpg"
                            }
                        ],
                        "pictures": [
                            {
                                "width": 500,
                                "height": 500,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_4/97608/pre/112_68sb.jpg"
                            }
                        ],
                        "price": {
                            "type": 1,
                            "min": 75,
                            "max": 100,
                            "not_available": "temp"
                        }
                    }
                ],
                "description": 
                    "Завершающий этап идеального маникюра. Защитное покрытие LE TOP COAT способствует 
                     быстрому высыханию лака, придает экстремальный блеск, защищает и продлевает стойкость лака для ногтей.\n\n
                     Наносите защитное покрытие LE TOP COAT поверх лака для создания идеального маникюра.",
                "sharing_url": "http://iledebeaute.ru/shop/sets/fragrance-woman/nabor_ange_ou_demon_le_secret;2hbk/"
            }

    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "description": "товар",
                "properties": {
                    "id": {
                        "type": "integer",
                        "description": "идентификатор товарной группы"
                    },
                    "brand": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 128,
                        "description": "название бренда"
                    },
                    "name": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 128,
                        "description": "название товарной группы"
                    },
                    "text": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 128,
                        "description": "описание товарной группы"
                    },
                    "pictures": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список картинок",
                        "items": {
                            "type": "object",
                            "description": "описание картинки",
                            "properties": {
                                "width": {
                                    "type": "integer",
                                    "minimum": 10,
                                    "maximum": 5000,
                                    "description": "ширина картинки"
                                },
                                "height": {
                                    "type": "integer",
                                    "minimum": 10,
                                    "maximum": 5000,
                                    "description": "высота картинки"
                                },
                                "url": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 1024,
                                    "description": "пусть к картинке"
                                }
                            },
                            "additionalProperties": false,
                            "required": ["width", "height", "url"]
                        }
                    },
                    "products": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "продукты в товарной группе",
                        "items": {
                            "type": "object",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор продукта"
                                },
                                "extra": {
                                    "type": "string",
                                    "minLength": 0,
                                    "maxLength": 128,
                                    "description": "строка с доп. текстом: название оттенка, аксессуара и прочее"
                                },
                                "volume": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 16,
                                    "description": "объем"
                                },
                                "photos": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок (фото продукта)",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок (штрих или квадрат с цветом)",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "price": {
                                    "type": "object",
                                    "description": "описание цены",
                                    "properties": {
                                        "type": {
                                            "type": "integer",
                                            "enum": [1, 2, 3],
                                            "description": "тип цены на товар: 1 - диапазон цен, возможна скидка по карте, 2 - фиксированная скидка, 3 - фиксированная цена"
                                        },
                                        "min": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "минимальная цена"
                                        },
                                        "max": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "максимальная цена"
                                        },
                                        "not_available": {
                                            "type": "string",
                                            "enum": ["temp", "perm"],
                                            "description": "'временно нет в наличии' или 'нет в продаже', если параметр не определен, то товар доступен для покупки"
                                        }
                                    },
                                    "additionalProperties": false,
                                    "required": ["type", "min", "max"]
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "extra", "photos", "pictures", "price"]
                        }
                    },
                    "description": {
                        "type": "string",
                        "minLength": 0,
                        "maxLength": 4096,
                        "description": "описание"
                    },
                    "sharing_url": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 1024,
                        "format": "uri",
                        "description": "ссылка на страницу товарной группы для шаринга"
                    }
                },
                "additionalProperties": false,
                "required": ["id", "brand", "name", "text", "pictures", "products", "description"]
            }

+ Response 401 (application/json)

+ Response 404 (application/json)

# Group Dior

## /dior/catalog

### GET - получение каталога Dior [GET]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)

    + Body

            {
                "catalog": [
                    {
                        "id": 1,
                        "name": "ЖЕНСКИЕ АРОМАТЫ",
                        "subs": [
                            {
                                "id": 20101,
                                "name": "J`Adore"
                            },
                            {
                                "id": 20102,
                                "name": "Les Escales"
                            },
                            {
                                "id": 20103,
                                "name": "Les Poisons"
                            },
                            {
                                "id": 20104,
                                "name": "Miss Dior"
                            }
                        ]
                    },
                    {
                        "id": 2,
                        "name": "МАКИЯЖ",
                        "subs": [
                            {
                                "id": 201,
                                "name": "Лицо",
                                "subs": [
                                    {
                                        "id": 20101,
                                        "name": "Тональные средства"
                                    },
                                    {
                                        "id": 20102,
                                        "name": "Корректоры"
                                    },
                                    {
                                        "id": 20103,
                                        "name": "Пудры"
                                    },
                                    {
                                        "id": 20104,
                                        "name": "Румяна"
                                    },
                                    {
                                        "id": 20105,
                                        "name": "Солнечный макияж"
                                    },
                                    {
                                        "id": 20106,
                                        "name": "Праймеры"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
    
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "catalog": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список категорий каталога 1 уровня",
                        "items": {
                            "type": "object",
                            "description": "категория каталога 1 уровня",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор категории"
                                },
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название категории"
                                },
                                "subs": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список подкатегорий второго уровня",
                                    "items": {
                                        "type": "object",
                                        "description": "подкатегория второго уровня",
                                        "properties": {
                                           "id": {
                                                "type": "integer",
                                                "description": "идентификатор подкатегории"
                                            },
                                            "name": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 128,
                                                "description": "название подкатегории"
                                            },
                                            "subs": {
                                                "type": "array",
                                                "uniqueItems": true,
                                                "description": "список подкатегорий третьего уровня",
                                                "items": {
                                                    "type": "object",
                                                    "description": "подкатегория третьего уровня",
                                                    "properties": {
                                                        "id": {
                                                            "type": "integer",
                                                            "description": "идентификатор подкатегории"
                                                        },
                                                        "name": {
                                                            "type": "string",
                                                            "minLength": 1,
                                                            "maxLength": 128,
                                                            "description": "название подкатегории"
                                                        }
                                                    },
                                                    "additionalProperties": false,
                                                    "required": ["id", "name"]
                                                }
                                            } 
                                        },
                                        "additionalProperties": false,
                                        "required": ["id", "name"]
                                    }
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "name", "subs"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["catalog"]
            }

+ Response 401 (application/json)

## /dior/catalog/banners
{?} Пока используются только первый баннер в массиве только для категорий первого уровня.
Возможно расширение до листалки баннеров на каждом из мест. Но только для категорий первого уровня.

### GET - получение баннеров для показа внутри каталога Dior [GET]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)

    + Body

            {
                "banners": [
                    {
                        "category_id": 1,
                        "id": 103,
                        "pictures": [
                            {
                                "width": 500,
                                "height": 500,
                                "url": "http://iledebeaute.ru/@/project/fit/539x322_sale_jan16.jpg"
                            }
                        ],
                        "action": {
                            // см. описание Actions 
                        }
                    },
                    {
                        "category_id": 101,
                        "id": 105,
                        "pictures": [
                            {
                                "width": 500,
                                "height": 500,
                                "url": "http://iledebeaute.ru/@/project/fit/tizer-im-abeille-190115.jpg"
                            }
                        ],
                        "action": {
                            // см. описание Actions
                        }
                    }
                ]
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "banners": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список баннеров",
                        "items": {
                            "type": "object",
                            "description": "баннер",
                            "properties": {
                                "category_id": {
                                    "type": "integer",
                                    "description": "идентификатор категории каталога"
                                },
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор баннера"
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "action": {
                                    // см. описание Actions
                                }
                            },
                            "additionalProperties": false,
                            "required": ["category_id", "id", "pictures", "action"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["banners"]
            }       

+ Response 401 (application/json)

## /dior/general
Метод получает баннер для главной страницы бренда и список товаров: Новинки. 
{?} Пока используется только первый из списка баннеров.

### GET - получение данных для главного экрана Dior [GET]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

+ Response 200 (application/json)

    + Body

            {
                "banners": [
                    {
                        "id": 103,
                        "pictures": [
                            {
                                "width": 500,
                                "height": 500,
                                "url": "http://iledebeaute.ru/@/project/fit/539x322_sale_jan16.jpg"
                            }
                        ],
                        "action": {
                            // см. описание Actions 
                        }
                    }
                ],
                "groups_new": [
                    {
                        "id": 1,
                        "brand": "DIOR",
                        "name": "Dior Addict Lip Glow Spring 2016",
                        "text": "Бальзам для губ",
                        "pictures": [
                            {
                                "width": 156,
                                "height": 156,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_5/115760/pre/156_156sb.jpg"
                            },
                            {
                                "width": 257,
                                "height": 257,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_5/115760/pre/257_257sb.jpg"
                            }
                        ],
                        "price": {
                            "type": 1,
                            "min": 75,
                            "max": 100,
                            "not_available": "temp"
                        }
                    }
                ]
            }

    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "banners": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список баннеров",
                        "items": {
                            "type": "object",
                            "description": "баннер",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор баннера"
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "action": {
                                    // см. описание Actions
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "pictures", "action"]
                        }
                    },
                    "groups_new": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список товарных групп в блоке Новинки",
                        "items": {
                            "type": "object",
                            "description": "группа",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор товарной группы"
                                },
                                "brand": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название бренда"
                                },
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название товарной группы"
                                },
                                "text": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "описание товарной группы"
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "price": {
                                    "type": "object",
                                    "description": "описание цены",
                                    "properties": {
                                        "type": {
                                            "type": "integer",
                                            "enum": [1, 2, 3],
                                            "description": "тип цены на товар: 1 - диапазон цен, возможна скидка по карте, 2 - фиксированная скидка, 3 - фиксированная цена"
                                        },
                                        "min": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "минимальная цена"
                                        },
                                        "max": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "максимальная цена"
                                        },
                                        "not_available": {
                                            "type": "string",
                                            "enum": ["temp", "perm"],
                                            "description": "'временно нет в наличии' или 'нет в продаже', если параметр не определен, то товар доступен для покупки"
                                        }
                                    },
                                    "additionalProperties": false,
                                    "required": ["type", "min", "max"]
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "brand", "name", "text", "pictures", "price"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["banners", "groups_new"]
            }       

+ Response 401 (application/json)

## /dior/catalog/groups
Группы товаров в категории каталога Dior.
Возвращает описания групп товаров страницами по 36 групп товаров на странице.
Коллекции должны работать аналогично категориям товаров.
{?} - на текущем этапе реализации приложения в списке **categories** можно передавать только один элемент: 
остальные не будут использоваться сервером при поиске.

### POST - получение списка групп товаров [POST]
+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==

    + Body

            {
                "categories": [
                    101
                ],
                "sort": "name",
                "page": 1 // необязательный параметр
            }
            
    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "categories": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список категорий",
                        "items": {
                            "type": "integer",
                            "description": "идентификатор категории",
                        }
                    },
                    "sort": {
                        "type": "string",
                        "enum": ["name", "cost_desc", "cost_asc", "brand"],
                        "description": "тип сортировки"
                    },
                    "page": {
                        "type": "integer",
                        "minimum": 1,
                        "description": "номер страницы результатов (по 36 товарных групп на странице)"
                    }
                },
                "additionalProperties": false,
                "required": ["categories", "sort"]
            }

+ Response 200 (application/json)
    + Body

            {
                "groups_count": 14,
                "sort": "name",
                "page": 1,
                "groups": [
                    {
                        "id": 1,
                        "brand": "DIOR",
                        "name": "Dior Addict Lip Glow Spring 2016",
                        "text": "Бальзам для губ",
                        "pictures": [
                            {
                                "width": 156,
                                "height": 156,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_5/115760/pre/156_156sb.jpg"
                            },
                            {
                                "width": 257,
                                "height": 257,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_5/115760/pre/257_257sb.jpg"
                            }
                        ],
                        "price": {
                            "type": 1,
                            "min": 75,
                            "max": 100,
                            "not_available": "temp"
                        },
                        "filters": [
                            412, // "новинка"
                            41 // "лимитированный выпуск"
                        ]
                    }
                ]
            }
    
    + Schema
    
            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "properties": {
                    "groups_count": {
                        "type": "integer",
                        "minimum": 1,
                        "description": "общее количество товарных групп в результатах поиска"
                    },
                    "sort": {
                        "type": "string",
                        "enum": ["name", "cost_desc", "cost_asc", "brand"],
                        "description": "тип сортировки"
                    },
                    "page": {
                        "type": "integer",
                        "minimum": 1,
                        "description": "номер страницы результатов (по 36 товарных групп на странице)"
                    },
                    "groups": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список товарных групп в данной категории",
                        "items": {
                            "type": "object",
                            "description": "группа",
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "description": "идентификатор товарной группы"
                                },
                                "brand": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название бренда"
                                },
                                "name": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "название товарной группы"
                                },
                                "text": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 128,
                                    "description": "описание товарной группы"
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "price": {
                                    "type": "object",
                                    "description": "описание цены",
                                    "properties": {
                                        "type": {
                                            "type": "integer",
                                            "enum": [1, 2, 3],
                                            "description": "тип цены на товар: 1 - диапазон цен, возможна скидка по карте, 2 - фиксированная скидка, 3 - фиксированная цена"
                                        },
                                        "min": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "минимальная цена"
                                        },
                                        "max": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "максимальная цена"
                                        },
                                        "not_available": {
                                            "type": "string",
                                            "enum": ["temp", "perm"],
                                            "description": "'временно нет в наличии' или 'нет в продаже', если параметр не определен, то товар доступен для покупки"
                                        }
                                    },
                                    "additionalProperties": false,
                                    "required": ["type", "min", "max"]
                                },
                                "filters": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список фильтров, в которых есть эта товарная группа",
                                    "items": {
                                        "type": "integer",
                                        "minimum": 1,
                                        "description": "идентификатор фильтра",
                                    }
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "brand", "name", "text", "pictures", "price", "filters"]
                        }
                    }
                },
                "additionalProperties": false,
                "required": ["groups_count", "sort", "page", "groups"]
            }

+ Response 400 (application/json)
    + Body

            {
                "errors": [
                    {
                        "code": 1,
                        "message": "Несоответствие запроса схеме данных"
                    },
                    {
                        "code": 23,
                        "message": "Не определен один из фильров или одна из категорий"
                    },
                    {
                        "code": 35,
                        "message": "Запрошенной страницы разультатов не существует"
                    }
                ]
            }

+ Response 401 (application/json)

## /dior/catalog/groups/{id}

### GET - получение списка продуктов в товарной группе [GET]
+ Parameters
    + id: 41241 (required, number) - ID товарной группы

+ Request (application/json)
    + Headers

            User-Agent: IledebeauteMobileApp (ios/9.2; mobile/1.2.3; iphone6splus) API/0.2
            Authorization: Basic ODozNTJlYTIyMTZlZDEzYmZhMDlhNTk0Z==
    
    
+ Response 200 (application/json)
    + Body

            {                
                "id": 88953,
                "brand": "DIOR",
                "name": "Dior Addict Lip Glow Spring 2016",
                "text": "Бальзам для губ",
                "pictures": [
                    {
                        "width": 156,
                        "height": 156,
                        "url": "http://static.iledebeaute.ru/files/images/tag/part_5/115760/pre/156_156sb.jpg"
                    },
                    {
                        "width": 500,
                        "height": 500,
                        "url": "http://static.iledebeaute.ru/files/images/tag/part_5/115760/pre/257_257sb.jpg"
                    }
                ],
                "products": [
                    {
                        "id": 10000,
                        "extra": "LE TOP COAT Защитное покрытие для ногтей, придающее блеск ",
                        "volume": "13 мл",
                        "pictures": [
                            {
                                "width": 500,
                                "height": 500,
                                "url": "http://static.iledebeaute.ru/files/images/tag/part_4/97608/pre/112_68sb.jpg"
                            }
                        ],
                        "price": {
                            "type": 1,
                            "min": 75,
                            "max": 100,
                            "not_available": "temp"
                        },
                        "filters": [
                            412, // "новинка"
                            41 // "лимитированный выпуск"
                        ]
                    }
                ],
                "description": 
                    "Lip Glow – революционный бальзам для губ, который реагирует на уровень влаги в губах и 
                    создает персональный естественный оттенок розового цвета. Основа макияжа и ухода Dior, 
                    этот вневременный продукт с универсальной формулой нашел свое место в весенней коллекции, 
                    которая посвящена сияющей коже. Питер Филипс воссоздал его в новом цветочном оттенке – 
                    мягкой лилии, преображающей любую внешность.",
                "sharing_url": "http://iledebeaute.ru/shop/sets/fragrance-woman/nabor_ange_ou_demon_le_secret;2hbk/"
            }

    + Schema

            {
                "$schema": "http://json-schema.org/draft-04/schema#",
                "type": "object",
                "description": "товар",
                "properties": {
                    "id": {
                        "type": "integer",
                        "description": "идентификатор товара"
                    },
                    "brand": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 128,
                        "description": "название бренда"
                    },
                    "name": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 128,
                        "description": "название товарной группы"
                    },
                    "text": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 128,
                        "description": "описание товарной группы"
                    },
                    "pictures": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "список картинок",
                        "items": {
                            "type": "object",
                            "description": "описание картинки",
                            "properties": {
                                "width": {
                                    "type": "integer",
                                    "minimum": 10,
                                    "maximum": 5000,
                                    "description": "ширина картинки"
                                },
                                "height": {
                                    "type": "integer",
                                    "minimum": 10,
                                    "maximum": 5000,
                                    "description": "высота картинки"
                                },
                                "url": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 1024,
                                    "description": "пусть к картинке"
                                }
                            },
                            "additionalProperties": false,
                            "required": ["width", "height", "url"]
                        }
                    },
                    "products": {
                        "type": "array",
                        "uniqueItems": true,
                        "description": "товары в группе",
                        "items": {
                            "type": "object",
                            "properties": {
                               "id": {
                                    "type": "integer",
                                    "description": "идентификатор продукта"
                                },
                                "extra": {
                                    "type": "string",
                                    "minLength": 0,
                                    "maxLength": 128,
                                    "description": "строка с доп. текстом: название оттенка, аксессуара и прочее"
                                },
                                "volume": {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 16,
                                    "description": "объем"
                                },
                                "pictures": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список картинок (штрих или квадрат с цветом)",
                                    "items": {
                                        "type": "object",
                                        "description": "описание картинки",
                                        "properties": {
                                            "width": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "ширина картинки"
                                            },
                                            "height": {
                                                "type": "integer",
                                                "minimum": 10,
                                                "maximum": 5000,
                                                "description": "высота картинки"
                                            },
                                            "url": {
                                                "type": "string",
                                                "minLength": 1,
                                                "maxLength": 1024,
                                                "description": "пусть к картинке"
                                            }
                                        },
                                        "additionalProperties": false,
                                        "required": ["width", "height", "url"]
                                    }
                                },
                                "price": {
                                    "type": "object",
                                    "description": "описание цены",
                                    "properties": {
                                        "type": {
                                            "type": "integer",
                                            "enum": [1, 2, 3],
                                            "description": "тип цены на товар: 1 - диапазон цен, возможна скидка по карте, 2 - фиксированная скидка, 3 - фиксированная цена"
                                        },
                                        "min": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "минимальная цена"
                                        },
                                        "max": {
                                            "type": "integer",
                                            "minimum": 1,
                                            "description": "максимальная цена"
                                        },
                                        "not_available": {
                                            "type": "string",
                                            "enum": ["temp", "perm"],
                                            "description": "'временно нет в наличии' или 'нет в продаже', если параметр не определен, то товар доступен для покупки"
                                        }
                                    },
                                    "additionalProperties": false,
                                    "required": ["type", "min", "max"]
                                },
                                "filters": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "description": "список фильтров, в которых есть этот продукт",
                                    "items": {
                                        "type": "integer",
                                        "minimum": 1,
                                        "description": "идентификатор фильтра",
                                    }
                                }
                            },
                            "additionalProperties": false,
                            "required": ["id", "extra", "pictures", "price", "filters"]
                        }
                    },
                    "description": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 4096,
                        "description": "описание"
                    },
                    "sharing_url": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 1024,
                        "format": "uri",
                        "description": "ссылка на страницу товара для шаринга"
                    }
                },
                "additionalProperties": false,
                "required": ["id", "brand", "name", "text", "pictures", "products", "description"]
            }

+ Response 401 (application/json)

+ Response 404 (application/json)